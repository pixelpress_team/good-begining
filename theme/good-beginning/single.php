<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post()?>
<div class="container article">		
	<?php if (has_post_thumbnail()):?>
	<div class="image">
		<?php the_post_thumbnail('thumb_780x525_true');?>
	</div>
	<?php endif;?>
	<div class="container content">
		<h2 class="hcups">Blog</h2>
		<h1 class="hbig"><?php the_title();?></h1>
		
		<ul class="a-listing">
			<li><?php the_category('</li><li>'); ?></li>
		</ul>
		
		<?php the_content();?>
	</div>

	<div class="share-box flex-wrap">
		<a href="#" id="leave-comment">Leave a comment</a>
		
		<div>
			<span>Share on</span>

			<ul>
				<li><a href="javascript: void(0)" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php the_permalink();?>','sharer','toolbar=0,status=0,width=700,height=400');">Facebook</a></li>
				<li><a href="javascript: void(0)"  onclick="window.open('https://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&media=<?php the_post_thumbnail_url( 'thumb_780x525_true' ); ?>&description=<?php the_title();?> <?php the_permalink();?>','sharer','toolbar=0,status=0,width=700,height=400');">Pinterest</a></li>
				<li><a href="javascript: void(0)" onclick="window.open('https://twitter.com/intent/tweet?text=<?php the_title();?> <?php the_permalink();?>','sharer','toolbar=0,status=0,width=700,height=400');">Twitter</a></li>
			</ul>
		</div>	
	</div>
	<div id="blog-comments">
		<?php comments_template(); ?>
	</div>
</div>
<?php endwhile; endif;?>
<div id="related-post" class="type-related">
	<?php $related_posts = km_rpbt_query_related_posts( get_the_ID(), 'category', array('posts_per_page' => -1));?>
		<?php if ($related_posts):?>
		<div class="container flex-wrap">
			<div class="heading">
				<h2 class="hbig h-90">Related Articles</h2>	
			</div>
			<div class="content-inner">
				<div class="posts-rows  flex-wrap">
					<?php $posts_count = 0;?>
					<?php foreach ($related_posts as $post): ?>
					<?php $posts_count++;?>
					<?php setup_postdata($post);?>
					<div class="card card-medium">
						<div class="image">
							<span><?php the_category(', '); ?></span>
							<?php the_post_thumbnail('thumb_325x441_true');?>
						</div>
						<div class="content">
							<a href="<?php the_permalink()?>" class="btn add-reg" data-text="Read more"><span>Read</span></a>
							<h3 class="hmedium heading-1"><?php echo strtolower(get_the_title());?></h3>
							<?php the_excerpt()?>
						</div>
					</div>
					<?php if($posts_count % 2 == 0 && $posts_count < count($related_posts)):?>
				</div>
				<div class="posts-rows flex-wrap" style="display: none">
					<?php endif;?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php else: ?>
		<div class="container flex-wrap">
			<div class="heading">
				<h2 class="hbig h-90">Related Articles</h2>	
			</div>
			<div class="content-inner">
				<div class="posts-rows  flex-wrap">
					<p>No related posts.</p>
				</div>
			</div>
		</div>
		<?php endif; wp_reset_postdata();?>
	
	<div class="container flex-wrap prev-next-box">
		<a href="/blog/" class="btn btn-big back-to-blog">Back to Blog</a>
		<a href="/blog/" class="btn btn-big more-articles">More Articles</a>
	</div>
</div>
<?php get_footer(); ?>
