<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title><?php wp_title(); ?></title>
		<?php wp_head(); ?>
	</head>
	<body <?php if(is_page_template('template-profile.php')){body_class('registry');} else {body_class();}?>>
		<header>
			<div class="topline">
				<div class="container">
					<?php if(get_field('registry_page_link', 'option')): ?>
					<a href="<?php the_field('registry_page_link', 'option')?>" class="findreg">Find a Registry</a>
					<?php endif;?>
					<?php if(!is_user_logged_in()):?>
					<?php wp_nav_menu( array('container' => false,
					 	'theme_location' => 'top-menu',
					 	'menu_id' => 'navigat',
					 	'menu_class' => 'navigation',
					 	'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					)); ?>
					<?php else:?>
						<?php $user = wp_get_current_user();?>
						<?php if ( in_array( 'charity', (array) $user->roles ) ) {
							wp_nav_menu( array('container' => false,
							 	'theme_location' => 'top-menu-logged-in-charity',
							 	'menu_id' => 'navigat',
							 	'menu_class' => 'navigation',
							 	'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						));
						} else {
							wp_nav_menu( array('container' => false,
							 	'theme_location' => 'top-menu-logged-in',
							 	'menu_id' => 'navigat',
							 	'menu_class' => 'navigation',
							 	'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						));
					}?>

					<?php endif;?>
				</div>
			</div>
			<div class="container">
				<div class="logo">
					<a href="<?php echo site_url();?>"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/TGB_MainLogo.svg" alt="Logo"></a>
				</div>
				<?php wp_nav_menu( array('container' => false,
				 	'theme_location' => 'primary',
				 	'menu_id' => 'navigation',
				 	'menu_class' => 'navigation',
				 	'items_wrap' => '<nav><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
				)); ?>
				<div class="nav">
				 	<button class="hamburger hamburger--spin js-hamburger" type="button">
					  	<span class="hamburger-box">
					    	<span class="hamburger-inner"></span>
					  	</span>
					</button>  
			        <div class="menu-mob">
				        <?php wp_nav_menu( array('container' => false,
						 	'theme_location' => 'primary-mobile',
						 	'menu_class' => 'menu-main',
						 	'items_wrap' => '<ul class="%2$s">%3$s</ul>',
						)); ?>
						<?php if(get_field('registry_page_link', 'option')): ?>
						<ul>
							<li><a href="<?php the_field('registry_page_link', 'option')?>" class="findreg">Find a Registry</a></li>
						</ul>
						<?php endif;?>
						<?php if(!is_user_logged_in()):?>
						<?php wp_nav_menu( array('container' => false,
						 	'theme_location' => 'top-menu',
						 	'menu_id' => 'navigat',
						 	'menu_class' => 'navigation',
						 	'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						)); ?>
						<?php else:?>
						<?php $user = wp_get_current_user();?>
						<?php if ( in_array( 'charity', (array) $user->roles ) ) {
								wp_nav_menu( array('container' => false,
								 	'theme_location' => 'top-menu-logged-in-charity',
								 	'menu_id' => 'navigat',
								 	'menu_class' => 'navigation',
								 	'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							));
							} else {
								wp_nav_menu( array('container' => false,
								 	'theme_location' => 'top-menu-logged-in',
								 	'menu_id' => 'navigat',
								 	'menu_class' => 'navigation',
								 	'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							));
						};?>
						<?php endif;?>
						<ul id="menu-find-mob" class="menu">
							<li class="menu-item">
								<a href="#info">search</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</header>
		<div class="modal fullview bgaccent noclose" id="searchBox">
			<div class="container">
				<div class="line_top">
					<div class="logo">
						<a href="<?php echo site_url()?>"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/TGB_MainLogo.svg" alt=""></a>
					</div>
					<a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>
				</div>
				<div class="content">
					<div id="registry" class="tab_panel">
						<form action="/registry/" method="post">
							<span class="hbig">Find A Registry</span>
							<input class="alternate" type="text" name="registry_name" placeholder="By name">
							<input class="btn btn-big" type="submit" value="+ Search">
						</form>
					</div>
					<div id="charity" class="tab_panel">
						<form action="/causes/" method="post">
							<span class="hbig">Find A Charity</span>
							<input class="alternate" type="text" name="charity_name_keyword" placeholder="By name or keyword">
							<input class="btn btn-big" type="submit" value="+ Search">
						</form>
					</div>
					<div id="info" class="tab_panel">
						<form action="<?php echo site_url();?>" method="get">
							<span class="hbig">Find Information</span>
							<input class="alternate" type="text" name="s" placeholder="By keyword">
							<input class="btn btn-big" type="submit" value="+ Search">
						</form>
					</div>
				</div>
				<div class="line_bottom">
					<div class="toright">
						<div class="flex-wrap menuline">
							<label>Search</label>
							<ul class="a-listing horizontal">
								<li><a href="#" class="tab" data-trigger="#registry">Registry</a></li>
								<li><a href="#" class="tab"		   data-trigger="#charity">Charity</a></li>
								<li><a href="#" class="tab"		   data-trigger="#info">Info</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>