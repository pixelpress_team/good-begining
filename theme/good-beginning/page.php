<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="container">
	<h1 class=hbig><?php the_title();?></h1>
		<div class="content">
			<?php the_content()?>
		</div>
</div>
<?php endwhile; endif;?>
<?php get_footer(); ?>