<?php
/*
Template Name: Home Template
*/
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="container" data-aos="fade-up">
	<div class="hero container-small">
		<?php if(get_field('promo_image')): ?>
		<div class="image" data-aos="fade-left">
			<?php echo wp_get_attachment_image( get_field('promo_image'), 'thumb_535x675_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('promo_sub-heading')):?>
				<h2 class="hcups"><?php the_field('promo_sub-heading');?></h2>
				<?php endif;?>
				<?php if(get_field('promo_heading')):?>
				<h1 class="hbig"><a href="<?php the_field('promo_cta_button_url');?>"><?php the_field('promo_heading');?></a></h1>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('promo_cta_button_url') && get_field('promo_cta_button_text')):?>	
			<a href="<?php the_field('promo_cta_button_url');?>" class="btn"><?php the_field('promo_cta_button_text');?></a>
			<?php endif;?>
		</div>
	</div>	
</div>
<?php get_template_part('parts/registries-search-form')?>
<div class="type1">
	<div class="container">
		<div class="container-small flex-wrap">
			<div class="heading">
				<h2 class="hbig h-90" data-aos="fade-right">How It Works</h2>
			</div>
			<div class="content" data-aos="fade-up">
				<?php if(get_field('hiw_image')): ?>
				<div class="image">
					<?php echo wp_get_attachment_image( get_field('hiw_image'), 'thumb_418x621_true' ); ?>
				</div>
				<?php endif;?>
				<div class="content-inner">
					<?php if(have_rows('hiw_list')):?>
					<div class="desktop">
					<?php while(have_rows('hiw_list')): the_row();?>
					<h3 class="hmedium"><span><?php echo wp_get_attachment_image( get_sub_field('icon'), 'thumb_44x48_true' ); ?></span><?php the_sub_field('title');?></h3>
					<?php the_sub_field('text');?>
					<?php endwhile;?>
					</div>
					<div id="block-for-slider" class="mobile">
				        <div id="viewport">
				            <ul id="slidewrapper">
				            	<?php $count_rows = 0;?>
				                <?php while(have_rows('hiw_list')): the_row();?>
				               	<?php $count_rows++;?>
				                <li class="slide">
				                	<h3 class="hmedium"><span><?php echo wp_get_attachment_image( get_sub_field('icon'), 'thumb_44x48_true' ); ?></span><?php the_sub_field('title');?></h3>
									<?php the_sub_field('text');?>
				                </li>
				                <?php endwhile;?>
				            </ul>
				            <div id="slide-count">1&nbsp;&nbsp;/&nbsp;&nbsp;<?php echo $count_rows;?></div>
				            <div id="prev-next-btns">
				                <div id="prev-btn"></div>
				                <div id="next-btn"></div>
				            </div>
				        </div>
				    </div>				    
					<?php endif;?>
					<?php if(get_field('hiw_cta_button_link') && get_field('hiw_cta_button_text')):?>
					<a href="<?php the_field('hiw_cta_button_link');?>" class="btn btn-big btn-margin"><?php the_field('hiw_cta_button_text');?></a>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($featured_chatity_posts_ids = get_field('fo_featured_chatity_posts')):?>
<?php $featured_chatity_posts = new WP_query(array(
	'post_type' => 'dd-charity',
	'showposts' => -1,
	'post__in' => $featured_chatity_posts_ids,
	'orderby' => 'post__in',
	'order' => 'ASC',
));?>
<?php if($featured_chatity_posts->have_posts()):?>
<div class="type2 bgaccent" id="featured_organizations" data-aos="fade-up">
	<div class="container">
		<h2 class="hbig">Featured Organizations</h2>	
		<div class="flex-wrap">
			<?php $count_posts = 0;?>
			<?php $count_rows = 0;?>
			<?php while($featured_chatity_posts->have_posts()): $featured_chatity_posts->the_post()?>
			<?php 
				if($count_rows % 2 == 0){
					if($count_posts % 2==0) {$class=" card-big"; $thumb_size = 'thumb_605x403_true';} else {$class=" card-small"; $thumb_size = 'thumb_296x400_true';}
				} else{ 
					if($count_posts % 2==0) {$class=" card-small"; $thumb_size = 'thumb_296x400_true';} else {$class=" card-big"; $thumb_size = 'thumb_605x403_true';}
				}
				if($count_posts % 2!=0){$count_rows++;}
				$count_posts++;
			?>
			<div class="card<?php echo $class;?>">
				<div class="image">
					<?php the_causes( get_the_ID() , '<span>',', ', '</span>');?>
					<?php if (has_post_thumbnail()){the_post_thumbnail($thumb_size);}?>
				</div>
				<div class="content" data-aos="fade-up" data-aos-duration="500">
					<?php if(is_user_logged_in()):?>
					<a href="<?php the_permalink();?>" class="btn add-reg" data-text="Add to Registry"><span>Add</span></a>
					<?php endif;?>
					<a href="<?php the_permalink();?>" class="hmedium heading-1"><?php echo strtolower( get_the_title() );?></a>
					<?php the_excerpt();?>
				</div>
			</div>
			<?php endwhile;?>
			
		</div>
	</div>
</div>
<?php endif; wp_reset_postdata();?>
<?php endif;?>
<div class="type3" data-aos="fade-up">
	<div class="container flex-wrap">
		<div class="content">
			<div class="content-inner">
				<div class="border-top">
					<?php the_field('of_text')?>
					<?php if(get_field('of_cta_button_link') && get_field('of_cta_button_text')):?>
					<a href="<?php the_field('of_cta_button_link');?>" class="btn btn-big"><?php the_field('of_cta_button_text');?></a>
					<?php endif;?>
				</div>
			</div>
			<?php if(get_field('of_image')): ?>
			<div class="image">
				<?php echo wp_get_attachment_image( get_field('of_image'), 'thumb_584x465_true' ); ?>
			</div>
			<?php endif;?>
		</div>
		<div class="heading">
			<h2 class="hbig h90">Our Founder</h2>
		</div>
		
	</div>
</div> 

<div class="type4" data-aos="fade-up">
	<div class="container">
		<?php if(get_field('fb_image')): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image'), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading')):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading');?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading')):?>
				<h2 class="hbig"><?php the_field('fb_heading');?></h2>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url') && get_field('fb_cta_button_text')):?>	
			<a href="<?php the_field('fb_cta_button_url');?>" class="btn"><?php the_field('fb_cta_button_text');?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php endwhile; endif;?>
<?php get_footer(); ?>
