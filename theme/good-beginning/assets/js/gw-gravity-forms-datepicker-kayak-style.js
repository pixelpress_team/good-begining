/**
 * Gravity Wiz // Gravity Forms // Kayak-style Datepicker
 * Style your Gravity Forms Datepicker like Kayak.com.
 */ 
// ( function( $ ) {
//     console.log(gform);

//     if( window.gform ) {

//         gform.addFilter( 'gform_datepicker_options_pre_init', function( options, formId, fieldId ) {

//             var className = 'datepicker-clean';

//             if( ! $( '#input_' + formId + '_' + fieldId ).parents( 'li.gfield' ).hasClass( className ) ) {
//                 return options;
//             }

//             var beforeShow = options.beforeShow,
//                 onClose    = options.onClose;

//             options.numberOfMonths = 2;
//             options.changeMonth = false;
//             options.changeYear = false;
//             options.showOtherMonths = true;
//             options.dayNamesMin = [ 'S', 'M', 'T', 'W', 'T', 'F', 'S' ];
//             options.beforeShow = function() {
//                 beforeShow();
//                 $( '#ui-datepicker-div' ).addClass( className );
//             };
//             options.onClose = function() {
//                 onClose();
//                 var closeInterval = setInterval( function() {
//                     var $dp = $( '#ui-datepicker-div' );
//                     if( ! $dp.is( ':visible' ) ) {
//                         $dp.removeClass( className );
//                         clearInterval( closeInterval );
//                     }
//                 }, 100 );
//             };

//             return options;
//         } );

//     }

// } )( jQuery );