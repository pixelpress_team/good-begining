if($==undefined){$=jQuery}
$(document).ready(function(){	
	var $root = $('html, body');
	$('.content a[href^="#"]:not(#loadMore):not(.btn-del):not(.btn-edit):not(#top):not(.gform_update_post_delete_link):not(#addToRegistry)').click(function() {
	    var href = $.attr(this, 'href');

	    $root.animate({
	        scrollTop: $(href).offset().top
	    }, 500, function () {
	        window.location.hash = href;
	    });

	    return false;
	});

	$('#menu-find a, #menu-find-mob a, .findreg').click(function(event) {
		event.preventDefault();
		$('#searchBox  .tab_panel, #searchBox .tab').removeClass('active');
		$('#searchBox').modal();
		var href = $.attr(this, 'href');
		$('#searchBox  .content '+ href).addClass('active');
		$('#searchBox  .a-listing [data-trigger^="'+ href +'"]').addClass('active');
		return false;
	});
	$('#searchBox .tab').click(function() {
		trigger = $(this).attr('data-trigger');
		$('#searchBox .tab').removeClass('active');
		$(this).addClass('active');
		$('#searchBox  .tab_panel').removeClass('active');
		$('#searchBox  .content '+ trigger).addClass('active');

	})


	$('#search-form button').click(function(){
		$('#search-form').submit();
	});
	$('#search-form #category .select-items div').click(function(){
		//console.log($('#search-form #category select').val());
		window.location = $('#search-form #category select').val();
	});
	$('#search-form #location-country .select-items div').click(function(){
		//console.log($('#search-form #category select').val());
		window.location = $('#search-form #location-country select').val();
	});
	$('#search-form #location-state .select-items div').click(function(){
		//console.log($('#search-form #category select').val());
		window.location = $('#search-form #location-state select').val();
	});
	$('#search-form #location-city .select-items div').click(function(){
		//console.log($('#search-form #category select').val());
		window.location = $('#search-form #location-city select').val();
	});
	$('#search-form #date .select-items div').click(function(){
		//console.log($('#search-form #category select').val());
		window.location = $('#search-form #date select').val();
	});
	
	// $('#search-registry').validetta({
	// 	bubblePosition: "bottom",
	// 	  	bubbleGapTop: 10,
	// 	  	bubbleGapLeft: -5,
	// });
	$('#leave-comment').click(function(event){
		event.preventDefault();
		$('#blog-comments').slideToggle('slow');

	});
	// $('footer ul.fmenu-line li:last-child a').click(function(event){
	// 	event.preventDefault();
	// });

	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$('.menu-mob').slideToggle('fast');
	})
	if ($(window).width() < 562) {
	   $('.fmenu h5').click( function(){
	   		$(this).next().slideToggle('slow');
	   })
	}


	$('.more-articles').click(function(event){
		event.preventDefault();
		$('#related-post .content-inner .posts-rows:hidden:first').slideDown('slow');
	})
	$('.next-organization').click(function(event){
		event.preventDefault();
		$('#related-organizations .content-inner .posts-rows:hidden:first').slideDown('slow');
	})

	// $('#donation-popup').click(function(event){
	// 	var charVal = $('#charities').val();
	// 	$('#DonationForm #charity-id-'+charVal).show();
	// 	var title = $('#DonationForm #charity-id-'+charVal+' .title').text();
	// 	$('#DonationForm #charity-id-'+charVal + ' #charitable_field_charity_post_name_element').val(title);
	// });
	$("#DonationForm").on($.modal.CLOSE, function(event, modal){
		$('#DonationForm').empty();
		// var charVal = $('#charities').val();
		// $('#DonationForm #charity-id-'+charVal).hide();
	});
	
	validate();
  	$('#search-registry input').on('keyup', validate);
	
	


	/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("charitable-form-field-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);
	
})

function validate() {
  var inputsWithValues = 0;
  
  // get all input fields except for type='submit'
  var myInputs = $("#search-registry input:not([type='submit'])");

  myInputs.each(function(e) {
    // if it has a value, increment the counter
    if ($(this).val()) {
      inputsWithValues += 1;
    }
  });

  if (inputsWithValues == myInputs.length) {
  	$('#search-registry button').click(function(){
		$('#search-registry').submit();
	});
	$("#search-registry button").removeClass('disabled');
    $("#search-registry button").prop("disabled", false);
  } else {
  	$("#search-registry button").addClass('disabled');
    $("#search-registry button").prop("disabled", true);
  }
}