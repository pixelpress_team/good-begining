jQuery.noConflict($);
/* Ajax functions */
jQuery(document).ready(function($) {
    //onclick
    $("#loadMore").on('click', function(e) {
        e.preventDefault();
        //init
        var that = $(this);
        var page = $(this).data('page');
        var category = $(this).data('category');
        var newPage = page + 1;
        var ajaxurl = that.data('url');

        //ajax call
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                page: page,
                category: category,
                action: 'ajax_script_load_more'

            },
            beforeSend : function(){
                $('#loadMore').addClass('loading');
            },
            error: function(response) {
                console.log(response);
                $('#loadMore').removeClass('loading');
            },
            success: function(response) {
                 $('#loadMore').removeClass('loading');
                //check
                if (response == 0) {
                    $('#ajax-content').append('<div class="text-center"><h3>You reached the end of the line!</h3><p>No more posts to load.</p></div>');
                    $('#loadMore').hide();
                } else {
                    that.data('page', newPage);
                    $('#ajax-content').append(response);
                }
            }
        });
    });
    $("#donation-popup").on('click', function(e) {
        e.preventDefault();
        
        var that = $(this);
        var registryId = $('#registry-id').text();
        var charVal = $('#charities').val();
        var ajaxurl = '/wp-content/themes/good-beginning/parts/ajax-donations.php';



        // //ajax call
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                campaign_id: charVal,
                action: 'ajax_script_load_donate_form'

            },
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
                //check
                $('#DonationForm').append(response); 
                $('input[name="registry_post_id"]').val(registryId);              
            }
        });

    });

    $("#addToRegistry").on('click', function(e) {
        e.preventDefault();
        var charity_id = $(this).attr('data-charityID');
        var ajaxurl = '/wp-content/themes/good-beginning/parts/ajax-add-charity.php';
        // //ajax call
        $.ajax({
            url: ajaxurl,           
            type: 'post',
            data: {
                charity_id: charity_id,
            },           
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
                console.log(response);
                $('#ajax-content').empty();
                $('#ajax-content').append(response);
                   // $('#ajax-content').append(response);

            }
        });

    });
});