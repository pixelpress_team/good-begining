if($==undefined){$=jQuery}
$(document).ready(function(){
	$('[data-showmore-target]').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		id = $(this).attr('data-showmore-target');
		$(this).text(function(i, text){
			return text === "Show more" ? "Show less" : "Show more";
		})
		$('#'+id).toggleClass('active');
	})
	$('[data-unhide]').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		id = $(this).attr('data-unhide');
		$(this).remove();
		$(id).removeClass('display-none');
	})
	$('.faq-list dt').click(function() {
		$(this).toggleClass('active');
		$(this).next('dd').toggleClass('active');
	})
	$('.steps > div').click(function() {
		$(this).toggleClass('active').siblings().removeClass('active');
	})
	// $('.faq-list dd').click(function() {
	// 	$(this).toggleClass('active');
	// 	$(this).prev('dt').toggleClass('active');
	// })

	$('.tab').click(function() {
		trigger = $(this).attr('data-trigger');
		$(this).addClass('active').siblings().removeClass('active');
		$(trigger).addClass('active').siblings().removeClass('active');
	})
})