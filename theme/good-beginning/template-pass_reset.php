<?php
/*
Template Name: Password Reset Template
*/
get_header(); ?>

<div class="type-content login">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<p class="hcups">Reset your password</p>
				<h1 class="hbig">Forgot your password</h1>
			</hgroup>
		</div>
		<div class="content">
			<?php the_content();?>
		</div>	
	</div>
</div>
<?php get_footer(); ?>
