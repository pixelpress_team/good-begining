<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

function the_slug_exists($post_name, $post_type) {
    global $wpdb;
    echo "SELECT post_name FROM ".$wpdb->prefix."posts WHERE post_name = '" . $post_name . "' AND post_type = '" . $post_type . "'" .PHP_EOL;
    if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "' AND post_type = '" . $post_type . "'", 'ARRAY_A')) {
        return true;
    } else {
        return false;
    }
}


add_action('wp_ajax_actionImportCSV', 'callbackImportCSV');
add_action('wp_ajax_nopriv_actionImportCSV', 'callbackImportCSV');
function callbackImportCSV(){
    $log ='';

    $filepath = $_POST['filepath'];

    if (($handle = fopen($filepath, "r")) !== FALSE) {
        $line_key = 0;
        $row = 0;
        while (($data = fgetcsv($handle, 1000, "`")) !== FALSE) {
            
            $row++;
        }
        echo $row;
        fclose($handle);
    }
    echo $script;
    // return  $arrResult;
    wp_die();
}




add_action('wp_ajax_actionImportCSVRow', 'callbackImportCSVRow');
add_action('wp_ajax_nopriv_actionImportCSVRow', 'callbackImportCSVRow');
function callbackImportCSVRow(){
    $i = 0;
    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_row($start_line, $n_lines, $filepath);

    foreach ($csv as $csv_row) {
        // $csv_row['CHARITY NAME']
        // $csv_row['CHARITY ID']
        // $csv_row['ADDRESSS']
        // $csv_row['EMAIL']
        // $csv_row['PHONE']
        // $csv_row['WEBSITE']
        // $csv_row['URL']

        $user_id = wp_insert_user( $userdata );
        $fp = fopen(__DIR__.'/import_report.csv', 'a');

        
        // cancel if data is missed
        if(( $csv_row['CHARITY ID'] == "" ) || ( $csv_row['EMAIL'] == "" ) || (strpos($csv_row['EMAIL'], '@') === false) ){
            fwrite($fp, '"'.$start_line.'"'.'`'.'""'.'`'.'"missed data"'.'`'.'""'.PHP_EOL);
            return;
            wp_die();
        }
        // cancel if user exists
        if(username_exists( $csv_row['EMAIL'] )){
            fwrite($fp, '"'.$start_line.'"'.'`'.'""'.'`'.'""'.'`'.'""'.'`'.'"user exists"'.PHP_EOL);
            return;   
            wp_die();
        }

        echo '<pre>';
        var_dump( $csv_row );
        echo '</pre>';
        // return;

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $password = substr(str_shuffle($permitted_chars), 0, 16);

        $role = 'charity';

        $userdata = array(
            //'ID'              => 0,
            'user_pass'       => $password, 
            'user_login'      => $csv_row['EMAIL'],
            'user_nicename'   => $csv_row['EMAIL'],
            'user_url'        => $csv_row['WEBSITE'],
            'user_email'      => $csv_row['EMAIL'],
            'display_name'    => $csv_row['EMAIL'],
            'nickname'        => '',
            'first_name'      => '',
            'last_name'       => '',
            'description'     => '',
            'rich_editing'    => 'true', // false - выключить визуальный редактор
            'user_registered' => '',
            'role'            => $role,
            'jabber'          => '',
            'aim'             => '',
            'yim'             => '',
        );

        // insert user
        $user_id = wp_insert_user( $userdata );
        $fp = fopen(__DIR__.'/import_report.csv', 'a');
        if( (get_class($user_id) !== 'WP_Error') && ( $user_id != 0 )  ){
            // update User data
            update_field('user_imported', 1 ,'user_'.$user_id);

            // save passwrod to the log file
            $post_update = array();
            $post_update['ID'] = $csv_row['CHARITY ID'];
            $post_update['post_author'] = $user_id;
            // update Charity post
            $post_id = wp_update_post( wp_slash($post_update) );
            update_field('address', $csv_row['ADDRESSS'] ,$post_id);
            update_field('phone', $csv_row['PHONE'] ,$post_id);


            
            if( $post_id ){
                // if post is updated
                fwrite($fp, '"'.$start_line.'"'.'`'.'"'.$csv_row['EMAIL'].'"'.'`'.'"'.$user_id.'"'.'`'.'"'.$password.'"'.'`'.'"'.$post_id.'"'.PHP_EOL);
            }else{
                // if post isn't updated
                fwrite($fp, '"'.$start_line.'"'.'`'.'"'.$csv_row['EMAIL'].'"'.'`'.'"'.$user_id.'"'.'`'.'"'.$password.'"'.'`'.'"post update failed"'.PHP_EOL);
            }
        }else{
            fwrite($fp, '"'.$start_line.'"'.'`'.'"'.$csv_row['EMAIL'].'"'.'`'.'"user failed"'.'`'.'""'.'`'.'""'.PHP_EOL);
        }


        fclose($fp);
    }
    wp_die();
}

function read_csv_update_row($start_line, $n_lines, $filepath ){   
    $heading_line = array(
        'CHARITY NAME',
        'CHARITY ID',
        'ADDRESSS',
        'EMAIL',
        'PHONE',
        'WEBSITE',
        'URL',
    );
    $csv = array();
    $tmpName = $filepath;
    if(($handle = fopen($tmpName, 'r')) !== FALSE) {
        // necessary if a large csv file
        $row = 0;
        while(($data = fgetcsv($handle, 100000, '`')) !== FALSE) {
            // number of fields in the csv
            $col_count = count($data);
            $row++;
            if( ( $row >= $start_line ) && ( $row <= ($start_line + $n_lines -1) ) ){
                // get the values from the csv
                foreach ($heading_line as $key => $heading_line_value) {
                    $csv[$row][$heading_line_value] = $data[$key];
                }
            }
        }
        fclose($handle);
    }
    return $csv;
}



function read_csv_update_rowCMPN($start_line, $n_lines, $filepath ){   
    $heading_line = array(
        'username',
        'user_id',
        'charity_id',
    );
    $csv = array();
    $tmpName = $filepath;
    if(($handle = fopen($tmpName, 'r')) !== FALSE) {
        // necessary if a large csv file
        $row = 0;
        while(($data = fgetcsv($handle, 100000, '`')) !== FALSE) {
            // number of fields in the csv
            $col_count = count($data);
            $row++;
            if( ( $row >= $start_line ) && ( $row <= ($start_line + $n_lines -1) ) ){
                // get the values from the csv
                foreach ($heading_line as $key => $heading_line_value) {
                    $csv[$row][$heading_line_value] = $data[$key];
                }
            }
        }
        fclose($handle);
    }
    return $csv;
}



add_action('wp_ajax_actionImportCSVRowCMPN', 'callbackImportCSVRowCMPN');
add_action('wp_ajax_nopriv_actionImportCSVRowCMPN', 'callbackImportCSVRowCMPN');
function callbackImportCSVRowCMPN(){
    $i = 0;
    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_rowCMPN($start_line, $n_lines, $filepath);
    $campaign_pt = 'campaign';

    foreach ($csv as $csv_row) {
        // $csv_row['username']
        // $csv_row['user_id']
        // $csv_row['charity_id']

        $fp = fopen(__DIR__.'/import_camps_report.csv', 'a');
        
        if( !is_null( get_page_by_title( get_the_title( $csv_row['charity_id'] ), 'OBJECT', $campaign_pt ) ) ){
            echo 'already imported';
        }else{
            $user_id = $csv_row['user_id'];
            $postdata = array(
                'post_status'   => 'publish',
                'post_title'    => get_the_title( $csv_row['charity_id'] ), 
                'post_type'     => $campaign_pt,
                'post_author'   => $user_id,
            );
            
            $campaign_id = wp_insert_post( $postdata );

            // echo '<pre>';
            // var_dump( $campaign_id );
            // echo '</pre>';
            // return;
            
            if( $campaign_id ){
                update_user_meta( $user_id, 'charity_id', $csv_row['charity_id'] );
                update_user_meta( $user_id, 'campaign_id', $campaign_id );    
                // var_dump( get_user_meta( $user_id, 'charity_id') );
                // var_dump( get_user_meta( $user_id, 'campaign_id') );
                fwrite($fp, '"'.$csv_row['username'].'"`"'.$user_id.'"`"'.$csv_row['charity_id'].'"`"'.$campaign_id.'"'.PHP_EOL);
            }else{
                fwrite($fp, '"'.$csv_row['username'].'"`"'.$user_id.'"`"'.$csv_row['charity_id'].'"`"campaign failed"'.PHP_EOL);
            }
            
        }
        fclose($fp);
        
    }
    wp_die();
}