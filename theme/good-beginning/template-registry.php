<?php
/*
Template Name: Registry Template
*/
get_header(); ?>
<?php get_template_part('parts/registries-search-form')?>
<?php 
global $paged;
$paged = get_query_var('paged');
if($_POST['registry_name']){
	$search =  $_POST['registry_name'];
} elseif( $_GET['fn'] && $_GET['ln'] ){
	$search = $_GET['fn'] .' '.$_GET['ln'];
}
$registries = new WP_Query(array(
	'post_type' => 'dd-registry',
	'posts_per_page' => 12,
	'post__not_in' => array(16684),
	'paged' => $paged,
	's' => $search,
));
if($registries->have_posts()):
?>
<div  id="pagination-anchor" class="type2" data-aos="fade-up">
	<div class="container">
		<?php if( $search ):?>
		<h1 class="hbig">Registry for <?php echo $search;?></h1>
		<?php else: ?>
		<h1 class="hbig">All Registries</h1>
		<?php endif;?>
		<div class="flex-wrap container-small">
			<?php while($registries->have_posts()): $registries->the_post();?>
			<div class="card card-medium">
				<div class="image">
					<?php 
					if(has_post_thumbnail()){
						the_post_thumbnail('thumb_292x400_true');
					} else {
						//echo '<img src="'.get_stylesheet_directory_uri().'/assets/images-content/featured_02.jpg" alt="">';
						echo '<img src="'.site_url().'/wp-content/uploads/2020/04/TGB_Contact-292x400.jpg" alt="">';
					}
					?>
					
				</div>
				<div class="content" data-aos="fade-up" data-aos-duration="500">
					<a href="<?php the_permalink()?>" class="btn" data-text="Read more"><span>Read more</span></a>
					<h3 class="hmedium heading-1"><a href="<?php the_permalink()?>"><?php the_title();?></a></h3>
					<?php //the_field('note')?>
					<?php the_registries_excerpt()?>
				</div>
			</div>
			<?php endwhile;?>
		</div>
	</div>
</div>
<div class="container pagination-box no-padding" data-aos="fade-up">
	<div class="pagination">
		<?php  
        if(function_exists('wp_pagenavi')) :
            custom_pagenavi( array( 'query' => $registries ) );
        endif;?>
	</div>
</div>
<?php else:?>
<div class="container type-404" data-aos="fade-up">
	<h1 class=hbig>Oops!</h1>
	<div class="flex-wrap">
		<p>It looks like the registry you're looking for is expired.</p>
		<a href="<?php echo site_url();?>" class="btn nobg">Back to homepage</a>	
	</div>
</div>
<?php endif;?>
<?php get_footer(); ?>
