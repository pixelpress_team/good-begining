<div class="card card-medium">
	<div class="image">
		<!-- <span><?php //the_category(', '); ?></span> -->
		<?php the_post_thumbnail('thumb_292x400_true');?>
	</div>
		<div class="content"  data-aos="fade-up" data-aos-duration="500">
		<a href="<?php the_permalink()?>" class="btn add-reg" data-text="Read more"><span>Read</span></a>
		<h3 class="hmedium heading-1"><a href="<?php the_permalink()?>"><?php echo strtolower( get_the_title());?></a></h3>
		<?php the_excerpt()?>
	</div>
</div>