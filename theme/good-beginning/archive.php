<?php get_header(); ?>
<?php get_template_part('parts/blog-search-form')?>
<?php if(have_posts()):?>
<div  id="pagination-anchor" class="type2">
	<div class="container">
		<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
		<?php /* If this is a category archive */ if (is_category()) { ?>
		<h2 class="hbig"><?php printf(__( '%s', 'base' ), single_cat_title('', false)); ?></h2>
		<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>			
		<h2 class="hbig"><?php printf(__( '%s', 'base' ), single_tag_title('', false)); ?></h2>
		<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		<h2 class="hbig"><?php the_time('F jS, Y'); ?></h2>
		<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		<h2 class="hbig"><?php the_time('F, Y'); ?></h2>
		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		<h2 class="hbig"><?php the_time('Y'); ?></h2>
		<?php /* If this is an author archive */ } elseif (is_author()) { ?>
		<h2 class="hbig"><?php _e('Author Archive', 'base'); ?></h2>
		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		<h2 class="hbig"><?php _e('Blog Archives', 'base'); ?></h2>
		<?php } ?>
		<div class="content-area">
        	<div class="flex-wrap container-small" >
				<?php while(have_posts()): the_post();?>
				<div class="card card-medium">
					<div class="image">
						<!-- <span><?php //the_category(', '); ?></span> -->
						<?php the_post_thumbnail('thumb_292x400_true');?>
					</div>
					<div class="content">
						<a href="<?php the_permalink()?>" class="btn" data-text="Read more"><span>Read more</span></a>
						<h3 class="hmedium heading"><?php echo strtolower( get_the_title());?></h3>
						<?php the_excerpt()?>
					</div>
				</div>
				<?php endwhile;?>
			</div>
		</div>
	</div>
</div>
<div class="container pagination-box">
	<div class="pagination">
		<?php  
        if(function_exists('wp_pagenavi')) :
            custom_pagenavi();
        endif;?>
	</div>
</div>
<?php endif;?>
<?php get_footer(); ?>