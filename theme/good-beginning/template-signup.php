<?php
/*
Template Name: Signup Template
*/
get_header(); ?>
<div class="type-content">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<div class="heading-top">
				<hgroup>
					<h2 class="hcups">Start a new registry</h2>
					<h3 class="hbig">Begin your journey by doing good.</h3>
				</hgroup>
				<p>Thank you for choosing The Good Beginning!</p>
				<p><strong>Please note that your registry automatically expires after one year.</strong></p>
				
				<!-- <a href="#SampleRegistry" class="btn btn-big" rel="modal:open">View a sample registry</a> -->
				<a href="<?php echo get_permalink(16684);?>" class="btn btn-big">View a sample registry</a>

			</div>
		</div>
		<div id="SampleRegistry" class="modal"><!-- add classs noclose to remove closing icon-->
			<!-- <iframe src="<?php echo get_template_directory_uri()?>/registry-sub.html"></iframe> -->
			<img src="<?php echo get_template_directory_uri()?>/assets/images-content/Registry-sub-copy.png" >
		</div>
		<div class="content signup">
			<?php the_content();?>
		</div>	
	</div>
</div>
<script>
	const $source = document.querySelector('#input_6_5');
	const $result = document.querySelector('#field_6_5 label');

	const typeHandler = function(e) {
	  if(e.target.value == ''){
	  	$result.innerHTML = 'THEGOODBEGINNING.COM/SAMPLE*';
	  } else {
	  	$result.innerHTML = 'THEGOODBEGINNING.COM/'+ e.target.value;
	  }
	}

	$source.addEventListener('input', typeHandler) // register for oninput
	$source.addEventListener('propertychange', typeHandler) // for IE8
</script>
<?php get_footer(); ?>
