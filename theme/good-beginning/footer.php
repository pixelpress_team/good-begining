		<footer class="bgaccent" data-aos="fade-up">
			<div class="container">
				<div class="one-half left">
					<div class="fmenu">
						<?php dynamic_sidebar('footer-menus-sidebar'); ?>
					</div>

					<?php wp_nav_menu( array('container' => false,
					 	'theme_location' => 'footer-copyrights-menu',
					 	'menu_id' => 'copyrights',
					 	'menu_class' => 'navigation',
					 	'items_wrap' => '<nav id="%1$s" ><ul class="%2$s fmenu-line">%3$s</ul></nav>',
					)); ?>
				</div>
				<div class="one-half right">
					<?php if(get_field('instagram_cta_text', 'option') && get_field('instagram_cta_url', 'option')):?>
					<a href="<?php the_field('instagram_cta_url', 'option')?>" class="hmedium" target="_blank"><?php the_field('instagram_cta_text', 'option')?></a>
					<?php endif;?>
					<?php dynamic_sidebar('instagram-sidebar'); ?>

					<?php wp_nav_menu( array('container' => false,
					 	'theme_location' => 'footer-copyrights-menu',
					 	'menu_id' => 'copyrights',
					 	'menu_class' => 'navigation',
					 	'items_wrap' => '<nav  id="%1$s"><ul class="%2$s fmenu-line">%3$s</ul></nav>',
					)); ?>
				</div>
			</div>			
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>