<?php
/*
Template Name: About Template
*/
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="container1010" data-aos="fade-up">
	<div class="hero hero-v2 container-small">
		<?php if(get_field('promo_image')): ?>
		<div class="image" data-aos="fade-left">
			<?php echo wp_get_attachment_image( get_field('promo_image'), 'thumb_768x517_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('promo_sub-heading')):?>
				<h1 class="hcups"><?php the_field('promo_sub-heading');?></h1>
				<?php endif;?>
				<div class="hlinks">
					<h2 class="hbig"><a href="#our-story">Our Story</a></h2>
					<h2 class="hbig"><a href="#press-features">Press Features</a></h2>
				</div>
			</hgroup>	
			<?php if(get_field('promo_cta_button_url') && get_field('promo_cta_button_text')):?>	
			<!-- <a href="<?php the_field('promo_cta_button_url');?>" class="btn"><?php the_field('promo_cta_button_text');?></a> -->
			<?php endif;?>
		</div>
	</div>	
</div>

<div id="our-story" class="type3-v2 bgaccent">
	<div class="container1010 flex-wrap">
		<div class="content">
			<?php if(have_rows('os_list')): $count_items = 0;?>
			<div class="content-inner">
				<?php while(have_rows('os_list')): the_row();?>
				<div data-count="0<?php echo ++$count_items;?>.">
					<h3 class="hmedium"><?php the_sub_field('title')?></h3>
					<?php the_sub_field('text');?>
				</div>
				<?php endwhile;?>
			</div>
			<?php endif;?>
			<?php if(get_field('os_image')): ?>
			<div class="image">
				<?php echo wp_get_attachment_image( get_field('os_image'), 'thumb_435x594_true' ); ?>
			</div>
			<?php endif;?>
		</div>
		<div class="heading">
			<h2 class="hbig h90">Our Story</h2>
		</div>
		
	</div>
</div>

<div id="press-features" class="type5">
	<div class="container flex-wrap">
		<div class="heading">
			<hgroup>
				<?php if(get_field('pf_sub-heading')):?>
				<h2 class="hcups"><?php the_field('pf_sub-heading');?></h2>
				<?php endif;?>
				<?php if(get_field('pf_heading')):?>
				<h2 class="hbig"><?php the_field('pf_heading');?></h2>
				<?php endif;?>
			</hgroup>
			<a href="#" class="btn btn-big btn-margin" data-showmore-target="showmore_01">Show more</a>
		</div>
		<?php if(have_rows('press_items')):?>
		<div class="content">
			<ul class="logo-list" id="showmore_01">
				<?php while(have_rows('press_items')): the_row();?>
				<li><a href="<?php the_sub_field('url')?>" targe="_blank"><?php echo wp_get_attachment_image( get_sub_field('logo'), 'thumb_155x9999_true' ); ?></a></li>
				<?php endwhile;?>
			</ul>			
		</div>
		<?php endif;?>
	</div>
</div>

<div class="type4">
	<div class="container">
		<?php if(get_field('fb_image')): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image'), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading')):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading');?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading')):?>
				<h2 class="hbig"><?php the_field('fb_heading');?></h2>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url') && get_field('fb_cta_button_text')):?>	
			<a href="<?php the_field('fb_cta_button_url');?>" class="btn"><?php the_field('fb_cta_button_text');?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php endwhile; endif;?>
<?php get_footer(); ?>
