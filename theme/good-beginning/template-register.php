<?php
/*
Template Name: Register Template
*/
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="container1010 flex-to-right">
	<div class="hero hero-v2 container-small">
		<?php if(get_field('promo_image')): ?>
		<div class="image" data-aos="fade-left">
			<?php echo wp_get_attachment_image( get_field('promo_image'), 'thumb_768x517_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content" data-aos="fade-up">
			<hgroup>
				<?php if(get_field('promo_sub-heading')):?>
				<h1 class="hcups"><?php the_field('promo_sub-heading');?></h1>
				<?php endif;?>
				<div class="hlinks">
					<h2 class="hbig"><a href="#how-it-works">How It Works</a></h2>
					<h2 class="hbig"><a href="#faq">FAQ</a></h2>
				</div>
			</hgroup>
		</div>
	</div>	
</div>

<div id="how-it-works" class="type3-v2 bgaccent"  data-aos="fade-up">
	<div class="container flex-wrap">
		<div class="content">			
			<div class="content-inner wide">
				<?php if(have_rows('hiw_list')): $count_items = 0;?>
				<div class="steps" style="margin-bottom: 65px" data-aos="fade-up" data-aos-duration="500">
					<?php while(have_rows('hiw_list')): the_row();?>
					<div>
						<div class="arrow"></div><div class="hmedium" data-count="0<?php echo ++$count_items;?>."><?php the_sub_field('title')?><?php echo wp_get_attachment_image( get_sub_field('icon'), 'thumb_44x48_true' ); ?></div>
						<div class="expand">
							<?php the_sub_field('text');?>
						</div>
					</div>
					<?php endwhile;?>
				</div>
				<?php endif;?>
				<?php if(get_field('hiw_sample_link')): ?>
				<a href="<?php the_field('hiw_sample_link')?>" class="btn btn-big">View a sample registry</a>
				<?php endif;?>				
			</div>
			<?php if(get_field('hiw_image')): ?>
			<div class="image">
				<?php echo wp_get_attachment_image( get_field('hiw_image'), 'full' ); ?>
			</div>
			<?php endif;?>
		</div>
		<div class="heading" data-aos="fade-left">
			<h2 class="hbig h90">How It Works</h2>
		</div>
		
	</div>
</div>

<div class="type4"  data-aos="fade-up">
	<div class="container">
		<?php if(get_field('fb_image')): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image'), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading')):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading');?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading')):?>
				<h2 class="hbig"><?php the_field('fb_heading');?></h2>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url') && get_field('fb_cta_button_text')):?>	
			<a href="<?php the_field('fb_cta_button_url');?>" class="btn"><?php the_field('fb_cta_button_text');?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php if(have_rows('faq_list')):?>
<div id="faq" class="type-faq"  data-aos="fade-up">
	<div class="container">
		<h2 class="hbig">Frequently Asked Questions</h2>
		<dl class="faq-list">
			<?php while(have_rows('faq_list')): the_row();?>
			<dt><?php the_sub_field('quertion')?></dt>
			<dd><?php the_sub_field('answer')?></dd>
			<?php endwhile;?>
		</dl>
	</div>
</div>
<?php endif;?>
<?php endwhile; endif;?>
<?php get_footer(); ?>
