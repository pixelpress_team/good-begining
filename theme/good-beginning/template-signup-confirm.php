<?php
/*
Template Name: Signup Confirmation Template
*/
get_header(); ?>
<div class="type-content login">
	<div class="container flex-wrap">
		<div class="heading">
			<hgroup>
				<p class="hcups">Log in To review your registry</p>
				<h1 class="hbig" style="max-width: 400px;">Thank you for allowing us to be a part of your wedding.</h1>
			</hgroup>
		</div>
		<div class="content">
			<?php //if (!(current_user_can('level_0'))){ ?>
			<form action="" method="post" class="form-flex form-borders">
			<input type="text" name="log" id="log" class="size-full" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" placeholder="Username" />
			<input type="password" name="pwd" id="pwd" class="size-full"  placeholder="Password" />
			<div class="flex-wrap">
				<a href="<?php echo get_option('home'); ?>/wp-login.php?action=lostpassword" class="btn nobg noicon size-onehalf">forgot your password?</a>
				<input type="submit" name="submit" value="+ LOGIN" class="btn btn-big size-onehalf" />			    
			    <input type="hidden" name="action" value="custom_login_action" />
			</div>
			</form>
			<?php //}?>		
		</div>
	</div>
</div>

<?php get_footer(); ?>
