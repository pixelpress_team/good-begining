<?php
/*
Template Name: Causes Template
*/
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="container1010" data-aos="fade-up">
	<div class="hero container-small">
		<?php if(get_field('promo_image')): ?>
		<div class="image" data-aos="fade-left">
			<?php echo wp_get_attachment_image( get_field('promo_image'), 'thumb_535x675_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('promo_sub-heading')):?>
				<h2 class="hcups"><?php the_field('promo_sub-heading');?></h2>
				<?php endif;?>
				<?php if(get_field('promo_heading')):?>
				<h1 class="hbig"><?php the_field('promo_heading');?></h1>
				<?php endif;?>
			</hgroup>	
			<?php $all_causes = get_terms( array(
				'taxonomy' => 'dd-cause',
				'orderby' => 'name', 
				'order' => 'ASC', 
			));?>
			<?php if($all_causes):?>
			<div>
				<ul class="a-listing flex">
					<?php foreach($all_causes as $cause):?>
					<li><a href="<?php echo get_term_link( $cause );?>"><?php echo $cause->name;?></a></li>
					<?php endforeach;?>
				</ul>
			</div>
			<?php endif;?>
		</div>
	</div>	
</div>

<?php get_template_part('parts/cases-search-form')?>
<?php 
if ($_POST['charity_name_keyword']){
	$charity_name_keyword = $_POST['charity_name_keyword'];
}
global $paged;
$paged = get_query_var('paged');
$charities = new WP_Query(array(
	'post_type' => 'dd-charity',
	'posts_per_page' => 8,
	'paged' => $paged,
	's' => $charity_name_keyword, 
))?>
<?php if($charities->have_posts()):?>
<div id="pagination-anchor" class="type2" data-aos="fade-up">
	<div class="container">
		<h2 class="hbig">Featured Organizations</h2>	
		<div class="flex-wrap">
			<?php $count_posts = 0;?>
			<?php $count_rows = 0;?>
			<?php while($charities->have_posts()): $charities->the_post(); ?>
			<?php 
				if($count_rows % 2 == 0){
					if($count_posts % 2==0) {$class=" card-big"; $thumb_size = 'thumb_605x403_true';} else {$class=" card-small"; $thumb_size = 'thumb_296x400_true';}
				} else{ 
					if($count_posts % 2==0) {$class=" card-small"; $thumb_size = 'thumb_296x400_true';} else {$class=" card-big"; $thumb_size = 'thumb_605x403_true';}
				}
				if($count_posts % 2!=0){$count_rows++;}
				$count_posts++;
			?>
			<div class="card<?php echo $class;?>">
				<?php if (has_post_thumbnail()):?>
				<div class="image">
					<?php the_causes( get_the_ID() , '<span>',', ', '</span>');?>
					<?php the_post_thumbnail($thumb_size)?>
				</div>
				<?php else:?>
				<div class="image no-f-image">
					<div class="no-photo-text">
						No Photo
					</div>
				</div>
				<?php endif;?>
			<div class="content" data-aos="fade-up">
					<?php if(is_user_logged_in()):?>
					<a href="<?php the_permalink()?>" class="btn add-reg" data-text="Add to Registry"><span>Add</span></a>
					<?php endif;?>
					<h3 class="hmedium heading-1"><a href="<?php the_permalink()?>"><?php echo strtolower( get_the_title());?></a></h3>
					<?php the_excerpt();?>
				</div>
			</div>
			<?php endwhile?>

		</div>
	</div>
</div>
<div class="container pagination-box" data-aos="fade-up">
	<div class="pagination">
		<?php  
        if(function_exists('wp_pagenavi')) :
            custom_pagenavi( array( 'query' => $charities ) );
        endif;?>
	</div>
</div>
<?php else: ?>
<div id="pagination-anchor" class="type2" data-aos="fade-up">
	<div class="container">
		<h2 class="hbig">Not Found</h2>
	</div>
</div>
<?php endif; wp_reset_postdata()?>
	
<div class="type-content bgaccent" data-aos="fade-up">
	<?php get_template_part('parts/causes-footer-form')?>
</div>

<div class="type4" data-aos="fade-up">
	<div class="container">
		<?php if(get_field('fb_image')): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image'), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading')):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading');?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading')):?>
				<h2 class="hbig"><?php the_field('fb_heading');?></h2>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url') && get_field('fb_cta_button_text')):?>	
			<a href="<?php the_field('fb_cta_button_url');?>" class="btn"><?php the_field('fb_cta_button_text');?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php endwhile; endif;?>
<?php get_footer(); ?>
