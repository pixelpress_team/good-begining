<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<?php $primary_cause = get_primary_taxonomy_term(get_the_ID(), 'dd-cause')?>
<div class="container">
	<div class="hero container-small">
		<?php if (has_post_thumbnail()):?>
		<div class="image">
			<?php the_post_thumbnail('thumb_535x675_true');?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<h2 class="hcups"><?php echo $primary_cause['title']?> organization </h2>
				<h1 class="hbig"><?php echo strtolower(get_the_title());?></h1>
			</hgroup>	
			<a href="#main-content" class="btn">learn more</a>
		</div>
	</div>	
</div>
<div id="main-content" class="type-content bgaccent ">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">About</h2>
				<h3 class="hbig"><?php echo strtolower(get_the_title());?></h3>
			</hgroup>
		</div>
		<div class="content">
			<?php if(get_field('main_content') || get_field('serving') ): ?>
			<?php the_field('main_content');?>
			<?php if(get_field('serving')):?>
			<h4 class="hmedium">Serving</h4>			
			<?php $serving_arr = get_field('serving')?>
			<?php $main_parent = get_term_top_most_parent($serving_arr->term_id, 'location')?>
			<p>
				<?php 
					echo $main_parent->name;
				?>
			</p>
			<?php endif;?>
			<?php if(have_rows('for_more_information')): ?>
			<h4 class="hmedium">For More Information</h4>
			<ul class="a-listing">
				<?php while(have_rows('for_more_information')): the_row();?>
				<li><?php the_sub_field('line')?></li>
				<?php endwhile;?>
			</ul>
			<?php endif;?>
			<?php else: ?>
				<?php the_content();?>
			<?php endif;?>
			<?php if(is_user_logged_in()):?>
			<a href="#" id="addToRegistry" class="btn btn-big" data-charityID=<?php echo get_the_ID()?>>Add to Registry</a>
			<?php endif;?>
			<div id="ajax-content">
			</div>
		</div>	
	</div>
</div>
<?php endwhile; endif;?>
<div id="related-organizations" class="type-related">
	<?php 
	$charities_from_category = new WP_Query(array(
		'post_type' => 'dd-charity',
		'showposts' => -1,
		'dd-cause' => $primary_cause['slug'],
		// 'orderby' => 'rand' 
	));
	if($charities_from_category->have_posts()):
	?>
	<div class="container1010 flex-wrap">
		<div class="heading">
			<h2 class="hbig h-90"><?php echo $primary_cause['title']?></h2>	
		</div>
		<div class="content-inner">
			<div class="posts-rows  flex-wrap">
				<?php $posts_count = 0;?>
				<?php while($charities_from_category->have_posts()): $charities_from_category->the_post();?>
				<?php $posts_count++;?>
				<div class="card card-medium">
					<div class="image">
						<span><?php the_category(', '); ?></span>
						<?php the_post_thumbnail('thumb_325x441_true');?>
					</div>
					<div class="content">
						<a href="<?php the_permalink()?>" class="btn add-reg" data-text="Read more"><span>Add</span></a>
						<h3 class="hmedium heading-1"><?php echo strtolower(get_the_title());?></h3>
						<?php the_excerpt()?>
					</div>
				</div>
				<?php if($posts_count % 2 == 0 && $posts_count < $charities_from_category->post_count):?>
			</div>
			<div class="posts-rows flex-wrap" style="display: none">
					<?php endif;?>
				<?php endwhile;?>
			</div>
		</div>
	</div>
	<?php endif; wp_reset_postdata();?>
	<div class="container flex-wrap prev-next-box">
		<a href="<?php echo $primary_cause['url']?>" class="btn btn-big back-to-category">Back to causes</a>
		<a href="/causes/" class="btn btn-big next-organization">Next organization</a>
		<?php 
		// $next = get_previous_post_link( '%link', 'Next organization', true, '', 'dd-cause' ); 
		// echo str_replace( '<a ', '<a class="btn btn-big next-org" ', $next );
		?>
	</div>
</div>
<?php get_footer(); ?>