<?php
/*
Template Name: Login Template
*/
get_header(); ?>
<div class="type-content login">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<p class="hcups">To access your registry</p>
				<h1 class="hbig">Login with username and password</h1>
			</hgroup>
		</div>
		<div class="content">
			<?php if($_GET['result'] == 'failed'){
				$user_login = $_GET['username'];
				$error_message = "Login failed. Wrong password or user name?";
			}?>
			<?php if (!(current_user_can('level_0'))){ ?>
			<?php if($error_message){ echo '<p class="login-failed-message">'.$error_message.'</p>';}?>
			<form action="" method="post" class="form-flex form-borders">
				<input type="text" name="log" id="log" class="size-full" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" placeholder="Username" />
				<input type="password" name="pwd" id="pwd" class="size-full"  placeholder="Password" />
				<div class="submit flex-wrap">
					<a href="<?php echo get_permalink(16675)?>" class="btn nobg noicon size-onehalf">forgot your password?</a>
					<input type="submit" name="submit" value="+ LOGIN" class="btn btn-big size-onehalf" />			    
				    <input type="hidden" name="action" value="custom_login_action" />
				</div>
			</form>
			<?php } else {
				wp_redirect( home_url().'/profile/' ); 
				exit;
			}?>
		</div>	
	</div>
</div>
<?php get_footer(); ?>
