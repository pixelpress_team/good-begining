<?php get_header(); ?>
<?php get_template_part('parts/registries-search-form')?>
<div class="container-small type-404">
	<h1 class=hbig>Oops!</h1>
	<div class="flex-wrap">
		<p>It looks like the page you're looking is not found.</p>
		<a href="<?php echo site_url()?>" class="btn nobg">Back to homepage</a>	
	</div>
</div>
<?php get_footer(); ?>
