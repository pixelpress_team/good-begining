<?php get_header(); ?>
<?php get_template_part('parts/cases-search-form')?>
<?php if(have_posts()):?>
<div  id="pagination-anchor" class="type2">
	<div class="container">
		<h2 class="hbig"><?php single_term_title(); ?></h2>	
		<div class="flex-wrap">
			<?php $count_posts = 0;?>
			<?php $count_rows = 0;?>
			<?php while(have_posts()): the_post(); ?>
			<?php 
				if($count_rows % 2 == 0){
					if($count_posts % 2==0) {$class=" card-big"; $thumb_size = 'thumb_605x403_true';} else {$class=" card-small"; $thumb_size = 'thumb_296x400_true';}
				} else{ 
					if($count_posts % 2==0) {$class=" card-small"; $thumb_size = 'thumb_296x400_true';} else {$class=" card-big"; $thumb_size = 'thumb_605x403_true';}
				}
				if($count_posts % 2!=0){$count_rows++;}
				$count_posts++;
			?>
			<div class="card<?php echo $class;?>">
				<?php if (has_post_thumbnail()):?>
				<div class="image">
					<?php the_causes( get_the_ID() , '<span>',', ', '</span>');?>
					<?php the_post_thumbnail($thumb_size)?>
				</div>
				<?php else:?>
				<div class="image no-f-image">
					<div class="no-photo-text">
						No Photo
					</div>
				</div>
				<?php endif;?>
				<div class="content">
					<?php if(is_user_logged_in()):?>
					<a href="<?php the_permalink()?>" class="btn add-reg" data-text="Add to Registry"><span>Add</span></a>
					<?php endif;?>
					<h3 class="hmedium heading-1"><?php echo strtolower( get_the_title());?></h3>
					<?php the_excerpt();?>
				</div>
			</div>
			<?php endwhile?>

		</div>
	</div>
</div>
<div class="container pagination-box">
	<div class="pagination">
		<?php  
        if(function_exists('wp_pagenavi')) :
            custom_pagenavi();
        endif;?>
	</div>


	<div class="margin-center"><a href="#" class="btn btn-big" data-unhide="#section_01">Suggest a Charity</a></div>
</div>
<?php else:?>
<div class="container-small type-404">
	<h1 class=hbig>Oops!</h1>
	<div class="flex-wrap">
		<p>It looks like the charity you're looking for is expired.</p>
		<a href="<?php echo site_url()?>/causes/" class="btn nobg">Back to homepage</a>	
	</div>
</div>
<?php endif?>
<div class="type-content bgaccent display-none" id="section_01">
	<?php get_template_part('parts/causes-footer-form')?>
</div>
<div class="type4">
	<div class="container">
		<?php if(get_field('fb_image', 40)): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image', 40), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading', 40)):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading', 40);?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading', 40)):?>
				<h2 class="hbig"><?php the_field('fb_heading', 40);?></h2>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url', 40) && get_field('fb_cta_button_text', 40)):?>	
			<a href="<?php the_field('fb_cta_button_url', 40);?>" class="btn"><?php the_field('fb_cta_button_text', 40);?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php get_footer(); ?>
