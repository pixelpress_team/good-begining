<?php get_header(); ?>
<?php get_template_part('parts/blog-search-form')?>
<?php if(have_posts()):?>
<div  id="pagination-anchor" class="type2">
	<div class="container">
		<h2 class="hbig">Search Results for "<?php echo get_search_query()?>"</h2>
		<div class="content-area">
        	<div class="flex-wrap container-small" >
				<?php while(have_posts()): the_post();?>
				<div class="card card-medium">
					<div class="image">
						<!-- <span><?php //the_category(', '); ?></span> -->
						<?php if (has_post_thumbnail()):?>
							<?php the_post_thumbnail('thumb_292x400_true');?>
						<?php else:?>
							<img src="/wp-content/uploads/2020/04/TGB_Contact-292x400.jpg" alt="">
						<?php endif;?>
					</div>
					<div class="content">
						<a href="<?php the_permalink()?>" class="btn" data-text="Read more"><span>Read more</span></a>
						<h3 class="hmedium"><?php the_title();?></h3>
						<?php the_excerpt()?>
					</div>
				</div>
				<?php endwhile;?>
			</div>
		</div>
	</div>
</div>
<div class="container pagination-box">
	<div class="pagination">
		<?php  
        if(function_exists('wp_pagenavi')) :
            custom_pagenavi();
        endif;?>
	</div>
</div>
<?php else: ?>
<div class="type2">
	<div class="container">
		<h2 class="hbig"><?php _e('No posts found.', 'base'); ?></h2>
		<div class="content-area">
        	<p><?php _e('Try a different search?', 'base'); ?></p>
		</div>
	</div>
</div>
<?php endif;?>
<?php get_footer(); ?>