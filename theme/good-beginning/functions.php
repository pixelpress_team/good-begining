<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
require_once 'import/functions-import.php';

include( get_stylesheet_directory().'/widgets.php' );
/**
 * Disable automatic general feed link outputting.
 */
automatic_feed_links( false );
remove_action('wp_head', 'wp_generator');

function good_beginning_styles() {

	$timestamp = '';
	$style_path = '/assets/fonts/fonts.css';
	if( file_exists( get_stylesheet_directory().$style_path ) ) {
		$timestamp = filemtime(get_stylesheet_directory() . $style_path );
		wp_enqueue_style( 'fonts', get_stylesheet_directory_uri() . $style_path,  '', $timestamp );
	}
	// wp_enqueue_style( 'fonts', get_stylesheet_directory_uri() . '/assets/fonts/fonts.css' );
	wp_enqueue_style( 'modal', get_stylesheet_directory_uri() . '/assets/3rdparty/jquery.modal/jquery.modal.min.css' );
	wp_enqueue_style( 'validetta', get_stylesheet_directory_uri() . '/assets/styles/validetta.css' );
	wp_enqueue_style( 'main', get_stylesheet_directory_uri() . '/assets/styles/main.css' );
	wp_enqueue_style( 'fix', get_stylesheet_directory_uri() . '/assets/styles/fix.css' );
	wp_enqueue_style( 'hamburgers', get_stylesheet_directory_uri() . '/assets/styles/hamburgers.css' );
	wp_enqueue_style( 'slider', get_stylesheet_directory_uri() . '/assets/styles/slider.css' );
	wp_enqueue_style( 'mobile', get_stylesheet_directory_uri() . '/assets/styles/mobile.css' );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' );
		wp_enqueue_style( 'anm-style', get_stylesheet_directory_uri() . '/assets/styles/anm-style.css' );
	wp_enqueue_style( 'aos-style', get_stylesheet_directory_uri() . '/assets/styles/aos.css' );
}
add_action( 'wp_enqueue_scripts', 'good_beginning_styles' );

function good_beginning_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'matchheight', get_template_directory_uri() . '/assets/js/jquery.matchHeight.js', 0, '', true );	
	wp_enqueue_script( 'amn-js', get_template_directory_uri() . '/assets/js/anmjs.js', 0, '', true );
	wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/js/aos.js', 0, '', true );
	wp_enqueue_script( 'script_ajax', get_template_directory_uri() . '/assets/js/script_ajax.js', 0, '', true );	
	wp_enqueue_script( 'custom-select', get_template_directory_uri() . '/assets/js/custom-select.js', 0, '', true );
	wp_enqueue_script( 'inputfile', get_template_directory_uri() . '/assets/js/inputfile.js', 0, '', true );
	wp_enqueue_script( 'modal', get_template_directory_uri() . '/assets/3rdparty/jquery.modal/jquery.modal.min.js', 0, '', true );
	wp_enqueue_script( 'validetta', get_template_directory_uri() . '/assets/js/validetta.js', 0, '', true );
	if(is_front_page()){
		wp_enqueue_script( 'slider', get_template_directory_uri() . '/assets/js/slider.js', 0, '', true );
	}
	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', 0, '', true );
	wp_enqueue_script( 'theme', get_template_directory_uri() . '/assets/js/theme.js', 0, '', true );

}
add_action( 'wp_enqueue_scripts', 'good_beginning_scripts' );


function my_login_logo_one() { 

echo '<style type="text/css"> 
body.login div#login h1 a {
 	background-image: url("'. get_template_directory_uri().'/assets/images/svg-icons/TGB_WatermarkLogo.svg");
	padding-bottom: 0;
    background-size: 190px;
    width: 190px;
    height: 190px;
} 
#wp-submit{
	background-color: #c38d4d;
	border: none;
	border-radius: 0;
	box-shadow: none;
	text-shadow: none;
}
</style>';
} 
add_action( 'login_enqueue_scripts', 'my_login_logo_one' );

add_filter( 'login_headerurl', 'custom_loginlogo_url' );

function custom_loginlogo_url($url) {

     return site_url();

}


if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'id' => 'footer-menus-sidebar',
		'name' => 'Footer Menus Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="hmedium">',
		'after_title' => '</h5>'
	));
	register_sidebar(array(
		'id' => 'instagram-sidebar',
		'name' => 'Instagram Sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));	
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'thumb_35x38_true', 35, 38, true );
	add_image_size( 'thumb_44x48_true', 44, 48, true );
	add_image_size( 'thumb_155x9999_true', 155, 9999, false );
	add_image_size( 'thumb_292x400_true', 292, 400, true );
	add_image_size( 'thumb_296x400_true', 296, 400, true );
	add_image_size( 'thumb_325x441_true', 325, 441, true );
	add_image_size( 'thumb_418x621_true', 418, 621, true );
	add_image_size( 'thumb_435x594_true', 435, 594, true );
	add_image_size( 'thumb_435x696_true', 435, 696, true );
	add_image_size( 'thumb_535x675_true', 535, 675, true );
	add_image_size( 'thumb_569x449_true', 569, 449, true );
	add_image_size( 'thumb_584x465_true', 584, 465, true );
	add_image_size( 'thumb_605x403_true', 605, 403, true );
	add_image_size( 'thumb_768x517_true', 768, 517, true );
	add_image_size( 'thumb_780x525_true', 780, 525, true );
	add_image_size( 'thumb_1120x565_true', 1120, 565, true );
}

register_nav_menus( array(
	'top-menu' => 'Top Navigation',
	'top-menu-logged-in' => 'Top Navigation Logged In',
	'top-menu-logged-in-charity' => 'Top Navigation Logged In Charity',
	'primary' => 'Primary Navigation',
	'primary-mobile' => 'Primary Navigation (Mobile)',
	'footer-copyrights-menu' => 'Footer Copyrights Navigation',
) );


/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
        $css_classes = str_replace("current-menu-item", "active", $css_classes);
        $css_classes = str_replace("current-menu-parent", "active", $css_classes);
        return $css_classes;
}
add_filter('nav_menu_css_class', 'change_menu_classes');

add_filter( 'excerpt_length', function(){
	return 20;
} );

add_filter('excerpt_more', function($more) {
	return '...';
});

// Custom Excerpt function for Advanced Custom Fields
function the_registries_excerpt() {
	global $post;
	$text = get_field('note'); //Replace 'your_field_name'
	if ( '' != $text ) {
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$excerpt_length = 20; // 20 words
		$excerpt_more = apply_filters('excerpt_more', ' ' . '...');
		$text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
	}
	echo apply_filters('the_excerpt', $text);
}

/*
 * initial posts dispaly
 */
function script_load_more($args = array()) {

    //initial posts load
    echo '<div id="ajax-primary" class="content-area">';
        echo '<div id="ajax-content"  class="flex-wrap container-small" >';
            ajax_script_load_more($args);
        echo '</div>';
    echo '</div>';
        echo '<div class="flex-wrap prev-next-box">';
        echo '<a href="#" id="loadMore" data-category="'.$args['category'].'" data-page="1" data-url="'.admin_url("admin-ajax.php").'" class="btn btn-big">Load More</a>';
        echo '<a href="#top" id="back-to-top" class="btn btn-big">Back to top</a>';
        echo '</div>';
    
}

/*
 * create short code.
 */
add_shortcode('ajax_posts', 'script_load_more');

/*
 * load more script call back
 */
function ajax_script_load_more($args) {

    //init ajax
    $ajax = false;
    //check ajax call or not
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $ajax = true;
    }
    //number of posts per page default
    $num =  get_option( 'posts_per_page' );
    //page number
    if($_POST['post_keyword']){
		$search =  $_POST['post_keyword'];
	}
    $paged = $_POST['page'] + 1;
    if( $_POST['category'] ){
    	$category = $_POST['category'];
    } elseif( $_GET['category'] ) {
    	$category = $_GET['category'];
    }
    
    $keyword = $_GET['keyword'];
    $archive_url =  $_GET['archive'];
    if( $archive_url ){
    	$date = parse_url($archive_url, PHP_URL_PATH);
    	$date_arr = explode('/',$date);
	}
	$latest_post = get_posts("post_type=post&numberposts=1");
    //args
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'cat' => $category,
        's' => $keyword,
        'monthnum' => $date_arr[2],
        'year' => $date_arr[1],
        'posts_per_page' =>$num,
        'paged'=>$paged,
        's' => $search,
        'post__not_in' => array($latest_post[0]->ID),
    );
    //query
    $query = new WP_Query($args);
    //check
    if ($query->have_posts()):
        //loop articales
        while ($query->have_posts()): $query->the_post();
            //include articles template
            include 'ajax-content.php';
        endwhile;
    else:
        echo 0;
    endif;
    //reset post data
    wp_reset_postdata();
    //check ajax call
    if($ajax) die();
}

/*
 * load more script ajax hooks
 */
add_action('wp_ajax_nopriv_ajax_script_load_more', 'ajax_script_load_more');
add_action('wp_ajax_ajax_script_load_more', 'ajax_script_load_more');


/*
 * load more script call back
 */
function ajax_script_load_donate_form($args) {

    //init ajax
    $ajax = false;
    //check ajax call or not
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $ajax = true;
    }

//     //page number
//     if($_POST['charityId']){
// 		$search =  $_POST['charityId'];
	
    
// 	$charities_posts = get_posts(array(
// 		'post_type'   => 'dd-charity',
// 		'include'     => $couple_charities,
// 		'numberposts' => -1
// 	));

	// if(is_array(get_user_meta($post->post_author, 'stripe_connect_details', true))):
	// $campaign_id = get_user_meta( $post->post_author, 'campaign_id',true );
	// else:
	// $campaign_id = 16662;
	// endif;
	// echo do_shortcode('[charitable_donation_form campaign_id='.$campaign_id.']');

	//}
    // check ajax call
    if($ajax) die();
}

/*
 * load more script ajax hooks
 */
add_action('wp_ajax_nopriv_ajax_script_load_donate_form', 'ajax_script_load_donate_form');
add_action('wp_ajax_ajax_script_load_donate_form', 'ajax_script_load_donate_form');



if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

add_action( 'init', 'create_collection_taxonomies' );
function create_collection_taxonomies() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Locations', 'taxonomy general name' ),
    'singular_name' => _x( 'Location', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Locations' ),
    'all_items' => __( 'All Locations' ),
    'parent_item' => __( 'Parent Location' ),
    'parent_item_colon' => __( 'Parent Location:' ),
    'edit_item' => __( 'Edit Location' ), 
    'update_item' => __( 'Update Location' ),
    'add_new_item' => __( 'Add New Location' ),
    'new_item_name' => __( 'New Location Name' ),
    'menu_name' => __( 'Locations' ),
  ); 	

  register_taxonomy('location','dd-charity', array(
    'public'=>true,
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    ));
}

function the_causes( $post_id , $before, $sep, $after ){

	$causes_list = wp_get_post_terms( $post_id, 'dd-cause', array('fields' => 'names') );

	$html = $before;
	foreach ($causes_list as $key => $causes_item ) {
		if($key == 0){
			$html .= $causes_item;
		} else {
			$html .= $sep.$causes_item;
		}
	}
	$html .= $after;

	echo  $html;
}

function custom_pagenavi( $args = array() ) {
	if ( !is_array( $args ) ) {
		$argv = func_get_args();

		$args = array();
		foreach ( array( 'before', 'after', 'options' ) as $i => $key ) {
			$args[ $key ] = isset( $argv[ $i ]) ? $argv[ $i ] : '';
		}
	}

	$args = wp_parse_args( $args, array(
		'before' => '',
		'after' => '',
		'wrapper_tag' => 'ul',
		'wrapper_class' => '',
		'options' => array(),
		'query' => $GLOBALS['wp_query'],
		'type' => 'posts',
		'echo' => true
	) );

	extract( $args, EXTR_SKIP );

	$options = wp_parse_args( $options, PageNavi_Core::$options->get() );

	$instance = new PixelPress_PageNavi_Call( $args );

	list( $posts_per_page, $paged, $total_pages ) = $instance->get_pagination_args();

	if ( 1 == $total_pages && !$options['always_show'] )
		return;

	$pages_to_show = absint( $options['num_pages'] );
	$larger_page_to_show = absint( $options['num_larger_page_numbers'] );
	$larger_page_multiple = absint( $options['larger_page_numbers_multiple'] );
	$pages_to_show_minus_1 = $pages_to_show - 1;
	$half_page_start = floor( $pages_to_show_minus_1/2 );
	$half_page_end = ceil( $pages_to_show_minus_1/2 );
	$start_page = $paged - $half_page_start;

	if ( $start_page <= 0 )
		$start_page = 1;

	$end_page = $paged + $half_page_end;

	if ( ( $end_page - $start_page ) != $pages_to_show_minus_1 )
		$end_page = $start_page + $pages_to_show_minus_1;

	if ( $end_page > $total_pages ) {
		$start_page = $total_pages - $pages_to_show_minus_1;
		$end_page = $total_pages;
	}

	if ( $start_page < 1 )
		$start_page = 1;

	// Support for filters to change class names
	$class_names = array(
		'pages' => apply_filters( 'wp_pagenavi_class_pages', 'pages'),
		'first' => apply_filters( 'wp_pagenavi_class_first', 'first' ),
		'previouspostslink' => apply_filters( 'wp_pagenavi_class_previouspostslink', 'previouspostslink' ),
		'extend' => apply_filters( 'wp_pagenavi_class_extend', 'extend' ),
		'smaller' => apply_filters( 'wp_pagenavi_class_smaller', 'smaller' ),
		'page' => apply_filters( 'wp_pagenavi_class_page', 'page' ),
		'current' => apply_filters( 'wp_pagenavi_class_current', 'active'),
		'larger' => apply_filters( 'wp_pagenavi_class_larger', 'larger' ),
		'nextpostslink' => apply_filters( 'wp_pagenavi_class_nextpostslink', 'nextpostslink'),
		'last' => apply_filters( 'wp_pagenavi_class_last', 'last'),
	);

	$out = '';
	switch ( intval( $options['style'] ) ) {
		// Normal
		case 1:
			// Text
			if ( !empty( $options['pages_text'] ) ) {
				$pages_text = str_replace(
					array( "%CURRENT_PAGE%", "%TOTAL_PAGES%" ),
					array( number_format_i18n( $paged ), number_format_i18n( $total_pages ) ),
					__( $options['pages_text'], 'wp-pagenavi' ) );
				$out .= "<li class='{$class_names['pages']}'>$pages_text</li>";
			}

			// if ( $start_page >= 2 && $pages_to_show < $total_pages ) {
			// 	// First
			// 	$first_text = str_replace( '%TOTAL_PAGES%', number_format_i18n( $total_pages ), __( $options['first_text'], 'wp-pagenavi' ) );
			// 	$out .=''. $instance->get_single( 1, $first_text, array(
			// 		'class' => $class_names['first']
			// 	), '%TOTAL_PAGES%' ).'';
			// }

			// Previous
			if ( $paged > 1 && !empty( $options['prev_text'] ) ) {
				$out .=''. $instance->get_single( $paged - 1, $options['prev_text'], array(
					'class' => $class_names['previouspostslink'],
					'rel'   => 'prev'
				) ).'';
			} else {
				$out .= '<div></div>';
			}
			$out .= "<" . $wrapper_tag . " class='" . $wrapper_class . "' role='navigation'>";
			if ( $start_page >= 2 && $pages_to_show < $total_pages ) {
				if ( !empty( $options['dotleft_text'] ) )
					$out .= "<li class='{$class_names['extend']}'>{$options['dotleft_text']}</li>";
			}

			// Smaller pages
			$larger_pages_array = array();
			if ( $larger_page_multiple )
				for ( $i = $larger_page_multiple; $i <= $total_pages; $i+= $larger_page_multiple )
					$larger_pages_array[] = $i;

			$larger_page_start = 0;
			foreach ( $larger_pages_array as $larger_page ) {
				if ( $larger_page < ($start_page - $half_page_start) && $larger_page_start < $larger_page_to_show ) {
					$out .='<li>'. $instance->get_single( $larger_page, $options['page_text'], array(
						'class' => "{$class_names['smaller']} {$class_names['page']}",
						'title' => sprintf( __( 'Page %s', 'wp-pagenavi' ), number_format_i18n( $larger_page ) ),
					) ).'</li>';
					$larger_page_start++;
				}
			}

			if ( $larger_page_start )
				$out .= "<li class='{$class_names['extend']}'>{$options['dotleft_text']}</li>";

			// Page numbers
			$timeline = 'smaller';
			foreach ( range( $start_page, $end_page ) as $i ) {
				if ( $i == $paged && !empty( $options['current_text'] ) ) {
					$current_page_text = str_replace( '%PAGE_NUMBER%', number_format_i18n( $i ), $options['current_text'] );
					$out .= "<li aria-current='page' class='{$class_names['current']}'>$current_page_text</li>";
					$timeline = 'larger';
				} else {
					$out .='<li>'. $instance->get_single( $i, $options['page_text'], array(
						'class' => "{$class_names['page']} {$class_names[$timeline]}",
						'title' => sprintf( __( 'Page %s', 'wp-pagenavi' ), number_format_i18n( $i ) ),
					) ).'</li>';
				}
			}

			// Large pages
			$larger_page_end = 0;
			$larger_page_out = '';
			foreach ( $larger_pages_array as $larger_page ) {
				if ( $larger_page > ($end_page + $half_page_end) && $larger_page_end < $larger_page_to_show ) {
					$larger_page_out .='<li>'. $instance->get_single( $larger_page, $options['page_text'], array(
						'class' => "{$class_names['larger']} {$class_names['page']}",
						'title' => sprintf( __( 'Page %s', 'wp-pagenavi' ), number_format_i18n( $larger_page ) ),
					) ).'</li>';
					$larger_page_end++;
				}
			}

			if ( $larger_page_out ) {
				$out .= "<li class='{$class_names['extend']}'>{$options['dotright_text']}</li>";
			}
			$out .= $larger_page_out;

			if ( $end_page < $total_pages ) {
				if ( !empty( $options['dotright_text'] ) )
					$out .= "<li class='{$class_names['extend']}'>{$options['dotright_text']}</li>";
			}
			$out .= "</" . $wrapper_tag . ">";
			// Next
			if ( $paged < $total_pages && !empty( $options['next_text'] ) ) {
				$out .=''. $instance->get_single( $paged + 1, $options['next_text'], array(
					'class' => $class_names['nextpostslink'],
					'rel'   => 'next'
				) ).'';
			} else {
				$out .= '<div></div>';
			}

			// if ( $end_page < $total_pages ) {
			// 	// Last
			// 	$out .=''.$instance->get_single( $total_pages, __( $options['last_text'], 'wp-pagenavi' ), array(
			// 		'class' => $class_names['last'],
			// 	), '%TOTAL_PAGES%' ).'';
			// }
			break;

		// Dropdown
		case 2:
			$out .= '<form action="" method="get">'."\n";
			$out .= '<select size="1" onchange="document.location.href = this.options[this.selectedIndex].value;">'."\n";

			foreach ( range( 1, $total_pages ) as $i ) {
				$page_num = $i;
				if ( $page_num == 1 )
					$page_num = 0;

				if ( $i == $paged ) {
					$current_page_text = str_replace( '%PAGE_NUMBER%', number_format_i18n( $i ), $options['current_text'] );
					$out .= '<option value="'.esc_url( $instance->get_url( $page_num ) ).'" selected="selected" class="'.$class_names['current'].'">'.$current_page_text."</option>\n";
				} else {
					$page_text = str_replace( '%PAGE_NUMBER%', number_format_i18n( $i ), $options['page_text'] );
					$out .= '<option value="'.esc_url( $instance->get_url( $page_num ) ).'">'.$page_text."</option>\n";
				}
			}

			$out .= "</select>\n";
			$out .= "</form>\n";
			break;
	}
	//$out = $before . "<" . $wrapper_tag . " class='" . $wrapper_class . "' role='navigation'>\n$out\n</" . $wrapper_tag . ">" . $after;
	$out = $before . $out . $after;

	$out = apply_filters( 'wp_pagenavi', $out );

	if ( !$echo )
		return $out;

	echo $out;
}

class PixelPress_PageNavi_Call {

	protected $args;

	function __construct( $args ) {
		$this->args = $args;
	}

	function __get( $key ) {
		return $this->args[ $key ];
	}

	function get_pagination_args() {
		global $numpages;

		$query = $this->query;

		switch( $this->type ) {
			case 'multipart':
				// Multipart page
				$posts_per_page = 1;
				$paged = max( 1, absint( get_query_var( 'page' ) ) );
				$total_pages = max( 1, $numpages );
				break;
			case 'users':
				// WP_User_Query
				$posts_per_page = $query->query_vars['number'];
				$paged = max( 1, floor( $query->query_vars['offset'] / $posts_per_page ) + 1 );
				$total_pages = max( 1, ceil( $query->total_users / $posts_per_page ) );
				break;
			default:
				// WP_Query
				$posts_per_page = intval( $query->get( 'posts_per_page' ) );
				$paged = max( 1, absint( $query->get( 'paged' ) ) );
				$total_pages = max( 1, absint( $query->max_num_pages ) );
				break;
		}

		return array( $posts_per_page, $paged, $total_pages );
	}

	function get_single( $page, $raw_text, $attr, $format = '%PAGE_NUMBER%' ) {
		if ( empty( $raw_text ) )
			return '';

		$text = str_replace( $format, number_format_i18n( $page ), $raw_text );

		$attr['href'] = $this->get_url( $page ).'#pagination-anchor';

		return html( 'a', $attr, $text );
	}

	function get_url( $page ) {
		return ( 'multipart' == $this->type ) ? get_multipage_link( $page ) : get_pagenum_link( $page );
	}
}

function get_primary_taxonomy_term( $post = 0, $taxonomy = 'category' ) {
	if ( ! $post ) {
		$post = get_the_ID();
	}

	$terms        = get_the_terms( $post, $taxonomy );
	$primary_term = array();

	if ( $terms ) {
		$term_display = '';
		$term_slug    = '';
		$term_link    = '';
		if ( class_exists( 'WPSEO_Primary_Term' ) ) {
			$wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
			$wpseo_primary_term = $wpseo_primary_term->get_primary_term();
			$term               = get_term( $wpseo_primary_term );
			if ( is_wp_error( $term ) ) {
				$term_display = $terms[0]->name;
				$term_slug    = $terms[0]->slug;
				$term_link    = get_term_link( $terms[0]->term_id );
			} else {
				$term_display = $term->name;
				$term_slug    = $term->slug;
				$term_link    = get_term_link( $term->term_id );
			}
		} else {
			$term_display = $terms[0]->name;
			$term_slug    = $terms[0]->slug;
			$term_link    = get_term_link( $terms[0]->term_id );
		}
		$primary_term['url']   = $term_link;
		$primary_term['slug']  = $term_slug;
		$primary_term['title'] = $term_display;
	}
	return $primary_term;
}

add_action('init', function(){

  // not the login request?
  if(!isset($_POST['action']) || $_POST['action'] !== 'custom_login_action')
    return;

  // see the codex for wp_signon()
  $result = wp_signon();

  if(is_wp_error($result))
    wp_die('Login failed. Wrong password or user name?');

  // redirect back to the requested page if login was successful    
   if( in_array('couple', $result->roles)) {
   		header('Location: ' . site_url().'/profile/');
  		exit;
   } else {
   		header('Location: ' . site_url());
  		exit;
   }
 
});

add_action('wp_logout','ps_redirect_after_logout');
function ps_redirect_after_logout(){
 	wp_redirect( site_url() );
 	exit();
}

function custom_get_archives( $args = '' ) {
	global $wpdb, $wp_locale;

	$defaults = array(
		'type'            => 'monthly',
		'limit'           => '',
		'format'          => 'html',
		'before'          => '',
		'after'           => '',
		'show_post_count' => false,
		'echo'            => 1,
		'order'           => 'DESC',
		'post_type'       => 'post',
		'year'            => get_query_var( 'year' ),
		'monthnum'        => get_query_var( 'monthnum' ),
		'day'             => get_query_var( 'day' ),
		'w'               => get_query_var( 'w' ),
	);

	$parsed_args = wp_parse_args( $args, $defaults );

	$post_type_object = get_post_type_object( $parsed_args['post_type'] );
	if ( ! is_post_type_viewable( $post_type_object ) ) {
		return;
	}
	$parsed_args['post_type'] = $post_type_object->name;

	if ( '' == $parsed_args['type'] ) {
		$parsed_args['type'] = 'monthly';
	}

	if ( ! empty( $parsed_args['limit'] ) ) {
		$parsed_args['limit'] = absint( $parsed_args['limit'] );
		$parsed_args['limit'] = ' LIMIT ' . $parsed_args['limit'];
	}

	$order = strtoupper( $parsed_args['order'] );
	if ( $order !== 'ASC' ) {
		$order = 'DESC';
	}

	// this is what will separate dates on weekly archive links
	$archive_week_separator = '&#8211;';

	$sql_where = $wpdb->prepare( "WHERE post_type = %s AND post_status = 'publish'", $parsed_args['post_type'] );

	/**
	 * Filters the SQL WHERE clause for retrieving archives.
	 *
	 * @since 2.2.0
	 *
	 * @param string $sql_where Portion of SQL query containing the WHERE clause.
	 * @param array  $parsed_args         An array of default arguments.
	 */
	$where = apply_filters( 'getarchives_where', $sql_where, $parsed_args );

	/**
	 * Filters the SQL JOIN clause for retrieving archives.
	 *
	 * @since 2.2.0
	 *
	 * @param string $sql_join    Portion of SQL query containing JOIN clause.
	 * @param array  $parsed_args An array of default arguments.
	 */
	$join = apply_filters( 'getarchives_join', '', $parsed_args );

	$output = '';

	$last_changed = wp_cache_get_last_changed( 'posts' );

	$limit = $parsed_args['limit'];

	if ( 'monthly' == $parsed_args['type'] ) {
		$query   = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date $order $limit";
		$key     = md5( $query );
		$key     = "wp_get_archives:$key:$last_changed";
		$results = wp_cache_get( $key, 'posts' );
		if ( ! $results ) {
			$results = $wpdb->get_results( $query );
			wp_cache_set( $key, $results, 'posts' );
		}
		if ( $results ) {
			$after = $parsed_args['after'];
			foreach ( (array) $results as $result ) {
				$url = get_month_link( $result->year, $result->month );
				$url_mod = $result->year.'/'.$result->month;
				
				if ( 'post' !== $parsed_args['post_type'] ) {
					$url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
				}
				/* translators: 1: Month name, 2: 4-digit year. */
				$text = sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $result->month ), $result->year );
				if ( $parsed_args['show_post_count'] ) {
					$parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
				}
				$selected = is_archive() && (string) $parsed_args['year'] === $result->year && (string) $parsed_args['monthnum'] === $result->month;
				$output  .= custom_get_archives_link( $url_mod, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
			}
		}
	} elseif ( 'yearly' == $parsed_args['type'] ) {
		$query   = "SELECT YEAR(post_date) AS `year`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date) ORDER BY post_date $order $limit";
		$key     = md5( $query );
		$key     = "wp_get_archives:$key:$last_changed";
		$results = wp_cache_get( $key, 'posts' );
		if ( ! $results ) {
			$results = $wpdb->get_results( $query );
			wp_cache_set( $key, $results, 'posts' );
		}
		if ( $results ) {
			$after = $parsed_args['after'];
			foreach ( (array) $results as $result ) {
				$url = get_year_link( $result->year );
				if ( 'post' !== $parsed_args['post_type'] ) {
					$url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
				}
				$text = sprintf( '%d', $result->year );
				if ( $parsed_args['show_post_count'] ) {
					$parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
				}
				$selected = is_archive() && (string) $parsed_args['year'] === $result->year;
				$output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
			}
		}
	} elseif ( 'daily' == $parsed_args['type'] ) {
		$query   = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, DAYOFMONTH(post_date) AS `dayofmonth`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date), DAYOFMONTH(post_date) ORDER BY post_date $order $limit";
		$key     = md5( $query );
		$key     = "wp_get_archives:$key:$last_changed";
		$results = wp_cache_get( $key, 'posts' );
		if ( ! $results ) {
			$results = $wpdb->get_results( $query );
			wp_cache_set( $key, $results, 'posts' );
		}
		if ( $results ) {
			$after = $parsed_args['after'];
			foreach ( (array) $results as $result ) {
				$url = get_day_link( $result->year, $result->month, $result->dayofmonth );

				if ( 'post' !== $parsed_args['post_type'] ) {
					$url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
				}
				$date = sprintf( '%1$d-%2$02d-%3$02d 00:00:00', $result->year, $result->month, $result->dayofmonth );
				$text = mysql2date( get_option( 'date_format' ), $date );
				if ( $parsed_args['show_post_count'] ) {
					$parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
				}
				$selected = is_archive() && (string) $parsed_args['year'] === $result->year && (string) $parsed_args['monthnum'] === $result->month && (string) $parsed_args['day'] === $result->dayofmonth;
				$output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
			}
		}
	} elseif ( 'weekly' == $parsed_args['type'] ) {
		$week    = _wp_mysql_week( '`post_date`' );
		$query   = "SELECT DISTINCT $week AS `week`, YEAR( `post_date` ) AS `yr`, DATE_FORMAT( `post_date`, '%Y-%m-%d' ) AS `yyyymmdd`, count( `ID` ) AS `posts` FROM `$wpdb->posts` $join $where GROUP BY $week, YEAR( `post_date` ) ORDER BY `post_date` $order $limit";
		$key     = md5( $query );
		$key     = "wp_get_archives:$key:$last_changed";
		$results = wp_cache_get( $key, 'posts' );
		if ( ! $results ) {
			$results = $wpdb->get_results( $query );
			wp_cache_set( $key, $results, 'posts' );
		}
		$arc_w_last = '';
		if ( $results ) {
			$after = $parsed_args['after'];
			foreach ( (array) $results as $result ) {
				if ( $result->week != $arc_w_last ) {
					$arc_year       = $result->yr;
					$arc_w_last     = $result->week;
					$arc_week       = get_weekstartend( $result->yyyymmdd, get_option( 'start_of_week' ) );
					$arc_week_start = date_i18n( get_option( 'date_format' ), $arc_week['start'] );
					$arc_week_end   = date_i18n( get_option( 'date_format' ), $arc_week['end'] );
					$url            = add_query_arg(
						array(
							'm' => $arc_year,
							'w' => $result->week,
						),
						home_url( '/' )
					);
					if ( 'post' !== $parsed_args['post_type'] ) {
						$url = add_query_arg( 'post_type', $parsed_args['post_type'], $url );
					}
					$text = $arc_week_start . $archive_week_separator . $arc_week_end;
					if ( $parsed_args['show_post_count'] ) {
						$parsed_args['after'] = '&nbsp;(' . $result->posts . ')' . $after;
					}
					$selected = is_archive() && (string) $parsed_args['year'] === $result->yr && (string) $parsed_args['w'] === $result->week;
					$output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
				}
			}
		}
	} elseif ( ( 'postbypost' == $parsed_args['type'] ) || ( 'alpha' == $parsed_args['type'] ) ) {
		$orderby = ( 'alpha' == $parsed_args['type'] ) ? 'post_title ASC ' : 'post_date DESC, ID DESC ';
		$query   = "SELECT * FROM $wpdb->posts $join $where ORDER BY $orderby $limit";
		$key     = md5( $query );
		$key     = "wp_get_archives:$key:$last_changed";
		$results = wp_cache_get( $key, 'posts' );
		if ( ! $results ) {
			$results = $wpdb->get_results( $query );
			wp_cache_set( $key, $results, 'posts' );
		}
		if ( $results ) {
			foreach ( (array) $results as $result ) {
				if ( $result->post_date != '0000-00-00 00:00:00' ) {
					$url = get_permalink( $result );
					if ( $result->post_title ) {
						/** This filter is documented in wp-includes/post-template.php */
						$text = strip_tags( apply_filters( 'the_title', $result->post_title, $result->ID ) );
					} else {
						$text = $result->ID;
					}
					$selected = $result->ID === get_the_ID();
					$output  .= get_archives_link( $url, $text, $parsed_args['format'], $parsed_args['before'], $parsed_args['after'], $selected );
				}
			}
		}
	}
	if ( $parsed_args['echo'] ) {
		echo $output;
	} else {
		return $output;
	}
}

function custom_get_archives_link( $url, $text, $format = 'html', $before = '', $after = '', $selected = false ) {
	$text         = wptexturize( $text );
	$url          =  $url;
	$aria_current = $selected ? ' aria-current="page"' : '';

	if ( 'link' === $format ) {
		$link_html = "\t<link rel='archives' title='" . esc_attr( $text ) . "' href='$url' />\n";
	} elseif ( 'option' === $format ) {
		$selected_attr = $selected ? " selected='selected'" : '';
		$link_html     = "\t<option value='$url'$selected_attr>$before $text $after</option>\n";
	} elseif ( 'html' === $format ) {
		$link_html = "\t<li>$before<a href='$url'$aria_current>$text</a>$after</li>\n";
	} else { // custom
		$link_html = "\t$before<a href='$url'$aria_current>$text</a>$after\n";
	}

	/**
	 * Filters the archive link content.
	 *
	 * @since 2.6.0
	 * @since 4.5.0 Added the `$url`, `$text`, `$format`, `$before`, and `$after` parameters.
	 * @since 5.2.0 Added the `$selected` parameter.
	 *
	 * @param string $link_html The archive HTML link content.
	 * @param string $url       URL to archive.
	 * @param string $text      Archive text description.
	 * @param string $format    Link format. Can be 'link', 'option', 'html', or custom.
	 * @param string $before    Content to prepend to the description.
	 * @param string $after     Content to append to the description.
	 * @param bool   $selected  True if the current page is the selected archive.
	 */
	return apply_filters( 'get_archives_link', $link_html, $url, $text, $format, $before, $after, $selected );
}

/*
 * GRAVITY FORMS
 */

//auto login user when a new user is registered
function gb_login_user_after_user_registration($user_id, $config, $entry, $password) {

	$user = get_userdata( $user_id );
	$user_login = $user->user_login;

  	wp_signon( array(
		'user_login' 		=> $user_login,
		'user_password' =>  $password,
		'remember' 			=> false
  	) );

}


add_action( 'template_redirect', function() {
	if( is_page_template('template-signup-confirm.php') && is_user_logged_in() ){
		$registry_posts = new WP_Query(array(
			'post_type' => 'dd-registry',
			'post_status' => 'publish',
			'post_author' => get_current_user_id(),
			'showposts' => 1,
		));
		if($registry_posts->have_posts()): while($registry_posts->have_posts()): $registry_posts->the_post(); 
			wp_redirect( get_permalink(), 301 );
			exit;
		endwhile; endif;
	}
} );

add_action("gform_user_registered", "gb_login_user_after_user_registration", 10, 4);

//create a registry when a new user is registered
function gb_create_registry_after_user_registration($user_id, $config, $entry) {
// print_r($entry);
// die();
	$title = $entry["7"] . ' & ' . $entry["8"] ;
	if($entry["5"]) {
		$slug = sanitize_title($entry["5"]);
	} else {
		$slug = sanitize_title($title);
	}
	//setup post
	$post = array(
		'post_title' 	=> $title,
		'post_name'		=> $slug,
		'post_type'  	=> 'dd-registry',
		'post_status' => 'publish',
		'post_author' => $user_id,
	);

	$post_id = wp_insert_post($post);

	//update_field($selector, $value, [$post_id]);

	update_field('email', $entry["9"], $post_id);
	update_field('phone', $entry["10"], $post_id);
	update_field('event_type', $entry["14"], $post_id);
	update_field('wedding_date', $entry["15"], $post_id);
	update_field('wedding_location', $entry["16"], $post_id);
	update_field('wedding_website', $entry["17"], $post_id);
	update_field('note', $entry["18"], $post_id);
	generate_featured_image($entry["19"], $post_id);
	update_field('charity_1', $entry["26"], $post_id);
	update_field('charity_2', $entry["27"], $post_id);
	update_field('charity_3', $entry["24"], $post_id);

	// Auto post ( Unique File Date ).
     

	//if this fails I think we should email the site owner to tell her someone registered but we didn't create a registry
}
add_action("gform_user_registered", "gb_create_registry_after_user_registration", 10, 3);

//publish user registry once it is updated
function gb_after_registry_update($entry, $form) {

	$id = $entry['post_id'];
	if( 'publish' != get_post_status( $id ) ) {
		wp_publish_post( $id );
	} 

}
add_action( 'gform_after_submission_2', 'gb_after_registry_update', 10, 2 );

function generate_featured_image( $image_url, $post_id  ){

    $upload_dir = wp_upload_dir();
    $context = stream_context_create(array (
    'http' => array (
        'header' => 'Authorization: Basic ' . base64_encode("demo:demodemo")
    )
	));
    $image_data = file_get_contents($image_url, false, $context);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))
      $file = $upload_dir['path'] . '/' . $filename;
    else
      $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    $res2= set_post_thumbnail( $post_id, $attach_id );
}

add_action( 'gform_after_submission', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {
	if($form['id'] == 8){
		if($entry["3"]){
			update_field('charity_1', $entry["3"], $entry["10"]);
		}
		if($entry["7"]){
			update_field('charity_2', $entry["7"], $entry["10"]);
			}
		if($entry["8"]){
			update_field('charity_3', $entry["8"], $entry["10"]);
		}
	}
 
}

add_filter( 'gform_pre_render_8', 'populate_charity_posts' );
add_filter( 'gform_pre_validation_8', 'populate_charity_posts' );
add_filter( 'gform_pre_submission_filter_8', 'populate_charity_posts' );
add_filter( 'gform_admin_pre_render_8', 'populate_charity_posts' );
function populate_charity_posts( $form ) {
	global $charity_1;
	global $charity_2;
	global $charity_3;

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'select charity-list' ) === false ) {
            continue;
        }

        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $posts = get_posts( 'numberposts=-1&post_status=publish&post_type=dd-charity&orderby=title&order=ASC' );
 
        $choices = array();
 		$choices[] = array( 'text' => 'Clear choice', 'value' => 'clear');
        foreach ( $posts as $post ) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->ID );
            if($charity_1 == $post->ID){
            	$charity_1_title = $post->post_title;
            }
            if($charity_2 == $post->ID){
            	$charity_2_title = $post->post_title;
            }
            if($charity_3 == $post->ID){
            	$charity_3_title = $post->post_title;
            }
        }
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        switch ($field->placeholder) {
		    case 'Charity one':
		        if($charity_1_title){
		        	$field->placeholder = $charity_1_title;
				}
		        break;
		    case 'Charity two':
		    	if($charity_2_title){
		        	$field->placeholder = $charity_2_title;
		        }
		        break;
		    case 'Charity three':
		    	if($charity_3_title){
		        	$field->placeholder = $charity_3_title;
		        }
		        break;
		}
       
        $field->choices = $choices;
 
    }
 
    return $form;
}

function get_terms_by_level ($taxonomy){
	$top_level_terms = get_terms(array(
		'taxonomy' => $taxonomy,
		'parent' => 0,
		'hide_empty' => false, 
	));
	foreach( $top_level_terms as $top_level_term ){
		$terms_by_level['top_lvl'][] = $top_level_term;
		$second_level_terms = get_terms(array(
			'taxonomy' => $taxonomy,
			'parent' => $top_level_term->term_id,
			'hide_empty' => false, 
		));
		foreach( $second_level_terms as $second_level_term ){
			$terms_by_level['second_lvl'][] = $second_level_term;
			$third_level_terms = get_terms(array(
				'taxonomy' => $taxonomy,
				'parent' => $second_level_term->term_id,
				'hide_empty' => false, 
			));
			foreach( $third_level_terms as $third_level_term ){
				$terms_by_level['third_lvl'][] = $third_level_term;
			}
		}
	}
	return $terms_by_level;
}

function selected_locations(){
	$term = get_queried_object();
	$ancestors_ids[] = $term->term_id; 
	if(isset( $term->parent ) ) {
		$ancestors_ids[] = $term->parent; 
		$parent = get_term_by( 'id', $term->parent, 'location' );
		if(isset( $parent->parent ) ) {
			$ancestors_ids[] = $parent->parent;
		}
	} else {
		$ancestors_ids = false;
	}
	return $ancestors_ids;	
}

add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );
function wti_loginout_menu_link( $items, $args ) {
   if ($args->theme_location == 'top-menu-logged-in' || $args->theme_location == 'top-menu-logged-in-charity') {
         $items .= '<li class="right"><a href="'. wp_logout_url() .'">'. __("Log Out") .'</a></li>';
   }
   return $items;
}

add_action( 'wp_login_failed', 'action_function_name_2861' );
function action_function_name_2861( $username ){
	$referrer = site_url().'/login/';
    $referrer = add_query_arg('result', 'failed', $referrer);
    $referrer = add_query_arg('username', $username, $referrer);

	if(!empty($referrer)) :
        wp_redirect($referrer);
        exit;
    endif;
}


/**
 * Set the role of users who register through the Charitable
 * registration form shortcode.
 *
 * @param   array $values
 * @return  array $values
 */
function ed_charitable_set_user_registration_role( $values ) {

    if ( array_key_exists( 'role', $values ) ) {
        return $values;
    }

    /** 
     * Replace 'campaign_creator' with the role you would like 
     * the users to have. Note that the role must be
     * registered already.
     *
     * @see https://codex.wordpress.org/Function_Reference/add_role
     *
     * The 'campaign_creator' role is available if you have
     * Charitable Ambassadors installed and activated.     
     */ 
    $values['role'] = 'charity';

    return $values;

}

add_filter( 'charitable_registration_values', 'ed_charitable_set_user_registration_role' );


/**
 * This adds a field to the end of the "Your Details" section in the Profile form.
 *
 * In this example, we add a Birthday field, which is a date field. 
 *
 * @param   array                   $fields
 * @param   Charitable_Profile_Form $form
 */
function ed_add_user_field_to_profile_form( $fields, Charitable_Profile_Form $form ) {

	$fields['additional_links_field'] = [
		'type'      => 'textarea',
		'label'     => 'Additional links (one per row)',
		'priority'  => 13,
		'required'  => false,
		'value'     => isset( $_POST['additional_links_field'] ) ? $_POST['additional_links_field'] : '',
	];
	
	/* Add a checkbox. */
	$causes = get_terms( array( 'taxonomy' => 'dd-cause', 'hide_empty' => true ) );
	foreach($causes as $cause){
		$cause_options[$cause->term_id] = $cause->name;
	}

	$fields['causes_field'] = [
		'type'      => 'select',
		'label'     => 'Category',
		'priority'  => 14,
		'required'  => false,
		'value'     => isset( $_POST['causes_field'] ) ? $_POST['causes_field'] : '',
		'options'   => $cause_options,
	];

    $fields[ 'image_field' ] = array(
        'label'         => 'Picture',
        'type'          => 'picture',
        'priority'      => 17,
        'required'      => false,
        'fullwidth'     => true,
        'size'          => 70,
        'uploader'      => true,
        'max_uploads'   => 1,

        'value'         => $form->get_user_value( 'image_field' ),
        'data_type'     => 'meta',
        // 'page'          => 'campaign_details',
        'help'          => 'Upload one photo to display on your charity page as main image.',
    );

    return $fields;
}

add_filter( 'charitable_user_fields', 'ed_add_user_field_to_profile_form', 10, 2 );


function ed_add_instagram_field_to_profile_form( $fields, Charitable_Profile_Form $form ) {

    $fields['instagram'] = [
		'type'      => 'text',
		'label'     => 'Instagram',
		'priority'  => 47,
		'required'  => false,
		'value'     =>  $form->get_user_value( 'instagram' ),
	];


    return $fields;

}

add_filter( 'charitable_user_social_fields', 'ed_add_instagram_field_to_profile_form', 10, 2 );


function ed_remove_stripe_field_from_profile_form( $fields, Charitable_Profile_Form $form ) {

        unset($fields['stripe_connect']); 
        return $fields;

}

add_filter( 'charitable_user_social_fields', 'ed_remove_stripe_field_from_profile_form', 10, 2 );


function ed_charitable_update_user_registration ( $values ){

	// print_r($values);
	// die();
	
	$user_id = get_current_user_id();

	$args = array(
	    'post_type'  => 'dd-charity',
	    'post_status' => array('draft','publish'),
	    'author'     => $user_id,
	);
	

	$charity_existing_posts = get_posts($args);
	$charity_existing_posts_0 = $charity_existing_posts[0];
	if ($charity_existing_posts_0->ID){	

		$charity_existing_posts_0->post_title = $values['organisation'];

		$up_post = wp_update_post( $charity_existing_posts_0 );
		$causes_array = array((int)$values['causes_field']);		
		wp_set_object_terms( $charity_existing_posts_0->ID, $causes_array, 'dd-cause', false );
		set_post_thumbnail( $charity_existing_posts_0->ID , $values['image_field'] );
		if($values['description']){
			update_field('main_content', $values['description'], $charity_existing_posts_0->ID);
		}

		if($values['additional_links_field']){
			$field_key = 'field_5e663748a6510';
			$links = $values['additional_links_field'];
			$links_array = preg_split('/\s+/', $links);
			foreach ($links_array as $second_gen) {
			    $feature_value = '<a href="'.(string)$second_gen.'" target="_blank">'.(string)$second_gen.'</a>';
			    $value[] = array( 'field_5e66376fa6511' => $feature_value );
			}
			update_field( 'field_5e663748a6510', $value, $charity_existing_posts_0->ID );
		}
		wp_set_object_terms( $charity_existing_posts_0->ID, strtolower( $values['country']), 'location', false );
		if( $values['state'] && !term_exists( $values['state']) ){
			if($values['country'] == 'US'){
				$state_args = array(
					'parent' => 292, 
				);
			} elseif($values['country'] == 'CA') {
				$state_args = array(
					'parent' => 295, 
				);
			}
			wp_insert_term($values['state'], 'location', $state_args );
		}
		wp_set_object_terms( $charity_existing_posts_0->ID,  $values['state'], 'location', false );

		if( $values['city'] && !term_exists( $values['city']) ) {
			
			$term = get_term_by('name', $values['state'], 'location');
			$city_args = array(
				'parent' => $term->term_id, 
			);
			
			wp_insert_term( $values['city'], 'location',$city_args );
		}
		wp_set_object_terms( $charity_existing_posts_0->ID,  $values['city'], 'location', false );
	} else {
		$title = $values['organisation'];
		$slug = sanitize_title($title);
		//setup post
		$post = array(
			'post_title' 	=> $title,
			'post_name'		=> $slug,
			'post_type'  	=> 'dd-charity',
			'post_status' => 'draft',
			'post_author' => $user_id,
		);

		$post_id = wp_insert_post($post);

		$causes_array = array((int)$values['causes_field']);		
		wp_set_object_terms( $post_id, $causes_array, 'dd-cause', false );
		set_post_thumbnail( $post_id , $values['image_field'] );
		if($values['description']){
			update_field('main_content', $values['description'], $post_id);
		}
		if($values['additional_links_field']){
			$field_key = 'field_5e663748a6510';
			$links = $values['additional_links_field'];
			$links_array = preg_split('/\s+/', $links);
			foreach ($links_array as $second_gen) {
			    $feature_value = '<a href="'.(string)$second_gen.'" target="_blank">'.(string)$second_gen.'</a>';
			    $value[] = array( 'field_5e66376fa6511' => $feature_value );
			}
			update_field( 'field_5e663748a6510', $value, $post_id );
		}

		wp_set_object_terms( $post_id, strtolower( $values['country']), 'location', false );
		if( $values['state'] && !term_exists( $values['state']) ){
			if($values['country'] == 'US'){
				$state_args = array(
					'parent' => 292, 
				);
			} elseif($values['country'] == 'CA') {
				$state_args = array(
					'parent' => 295, 
				);
			}
			wp_insert_term($values['state'], 'location', $state_args );
		}
		wp_set_object_terms( $post_id,  $values['state'], 'location', false );

		if( $values['city'] && !term_exists( $values['city']) ) {
			
			$term = get_term_by('name', $values['state'], 'location');
			$city_args = array(
				'parent' => $term->term_id, 
			);
			
			wp_insert_term( $values['city'], 'location',$city_args );
		}
		wp_set_object_terms( $post_id,  $values['city'], 'location', false );

		update_user_meta( $user_id , 'charity_id', $post_id );		
	}

	$args_campaign = array(
	    'post_type'  => 'campaign',
	    'post_status' => array('draft','publish'),
	    'author'     => $user_id,
	);

	$campaign_existing_posts = get_posts($args_campaign);
	$campaign_existing_posts_0 = $campaign_existing_posts[0];
	if ($campaign_existing_posts_0->ID){	

		$campaign_existing_posts_0->post_title = $values['organisation'];

		$up_post_campaign = wp_update_post( $campaign_existing_posts_0 );
		if($values['description']){
			update_field('_campaign_description', $values['description'], $campaign_existing_posts_0->ID);
		}

	} else {
		//setup post
		$post_campaign = array(
			'post_title' 	=> $title,
			'post_type'  	=> 'campaign',
			'post_status' => 'publish',
			'post_author' => $user_id,
		);
		$post_campaign_id = wp_insert_post($post_campaign);
		if($values['description']){
			update_field('_campaign_description', $values['description'], $post_campaign_id);
		}
		update_user_meta( $user_id , 'campaign_id', $post_campaign_id );
		update_post_meta( $post_campaign_id, '_campaign_allow_custom_donations', 1 );
	}
}
add_filter( 'charitable_profile_update_values', 'ed_charitable_update_user_registration' );

/**
 * By default, Charitable will allow donors to select any country in the world
 * when they donate, update their profile or submit a fundraising campaign.
 *
 * In the examples below, we alter each of these forms to limit the list of accepted
 * countries to a specific few.
 *
 * @param  array $fields The fields in the form.
 * @return array
 */
// function ed_set_list_of_accepted_countries( $fields ) {
	// /* If a country field doesn't exist, stop right here. */
	// if ( ! array_key_exists( 'country', $fields ) ) {
	// 	return $fields;
	// }

	/**
	 * List of accepted countries.
	 *
	 * Note that these should be listed as an array where the
	 * key is the country code and the value is the name of the
	 * country.
	 *
	 * In the example below, we limit the list to just the Benelux
	 * countries.
	 */
// 	$options = [
// 		'CA' => 'Canada',
// 		'US' => 'United States',
// 	];

// 	$fields['country']['options'] = $options;

// 	return $fields;
// }

// add_filter( 'charitable_donation_form_user_fields', 'ed_set_list_of_accepted_countries' );
// add_filter( 'charitable_user_address_fields', 'ed_set_list_of_accepted_countries' );
// add_filter( 'charitable_campaign_submission_user_fields', 'ed_set_list_of_accepted_countries' );

/**
 * This example shows how to change the label of a field in a form.
 *
 * Our example below changes the "Address 2" line in the donation form
 * to "Address (continued)".
 *
 * Most of the forms that are used on the frontend in Charitable
 * include filters to allow you to change labels. Below, we list
 * out each form along with the hook you should use to alter it,
 * and the list of fields that are in that form by default:
 *
 * - Registration form: charitable_user_registration_fields
 * --- 'user_email'
 * --- 'user_login'
 * --- 'user_pass'
 *
 * - Profile form (user section): charitable_user_fields
 * --- 'first_name'
 * --- 'last_name'
 * --- 'user_email'
 * --- 'organisation'
 * --- 'description'
 *
 * - Profile form (address section): charitable_user_address_fields
 * --- 'address'
 * --- 'address_2'
 * --- 'city'
 * --- 'state'
 * --- 'postcode'
 * --- 'country'
 * --- 'phone'
 *
 * - Profile form (social section): charitable_user_social_fields
 * --- 'user_url'
 * --- 'twitter'
 * --- 'facebook'
 *
 * - Donation form (user section): charitable_donation_form_user_fields
 * --- 'first_name'
 * --- 'last_name'
 * --- 'email'
 * --- 'address'
 * --- 'address_2'
 * --- 'city'
 * --- 'state'
 * --- 'postcode'
 * --- 'country'
 * --- 'phone'
 * 
 * - Donation form (donation amount section): charitable_donation_form_donation_fields
 * --- 'donation_amount'
 * 
 * - Campaign form (campaign section): charitable_campaign_submission_campaign_fields
 * --- 'post_title'
 * --- 'description'
 * --- 'goal'
 * --- 'length'
 * --- 'post_content'
 * --- 'image'
 * --- 'campaign_category'
 * --- 'campaign_tag'
 *
 * - Campaign form (donation options section): charitable_campaign_submission_donation_options_fields
 * --- 'donation_options' 
 * --- 'suggested_donations'
 * --- 'allow_custom_donations' 
 *
 * - Campaign form (user section): charitable_campaign_submission_user_fields
 * --- 'first_name'
 * --- 'last_name'
 * --- 'user_email'
 * --- 'city'
 * --- 'state'
 * --- 'country'
 * --- 'user_description'
 * --- 'organisation'
 */
function ed_charitable_change_donation_form_labels( $fields ) {

    /**
     * Change the address field label.
     */
    $fields['description']['label'] = 'Description';

    return $fields;
}

add_filter( 'charitable_user_fields', 'ed_charitable_change_donation_form_labels' );

add_action( 'init', 'wpse16902_init' );
function wpse16902_init() {
    $GLOBALS['wp_rewrite']->use_verbose_page_rules = true;
}

add_filter( 'page_rewrite_rules', 'wpse16902_collect_page_rewrite_rules' );
function wpse16902_collect_page_rewrite_rules( $page_rewrite_rules )
{
    $GLOBALS['wpse16902_page_rewrite_rules'] = $page_rewrite_rules;
    return array();
}

add_filter( 'rewrite_rules_array', 'wspe16902_prepend_page_rewrite_rules' );
function wspe16902_prepend_page_rewrite_rules( $rewrite_rules )
{
    return $GLOBALS['wpse16902_page_rewrite_rules'] + $rewrite_rules;
}

function get_term_top_most_parent( $term_id, $taxonomy ) {
    $parent  = get_term_by( 'id', $term_id, $taxonomy );
    while ( $parent->parent != 0 ){
        $parent  = get_term_by( 'id', $parent->parent, $taxonomy );
    }
    return $parent;

}

add_action(
	'init',
	function() {
		/* Create the Donation Field instance. */
		$field = new Charitable_Donation_Field(
			'registry_post_id',
			array(
				'label'          => 'Registry Post ID',
				'data_type'      => 'meta',
				'value_callback' => false,
				'donation_form'  => array(
	                'type'       => 'hidden',
	                'required'   => true,
	                'show_after' => 'phone',
	            ), 
				'admin_form'     => true,
				'show_in_meta'   => true,
				'show_in_export' => true,
				'email_tag'      => false,
			)
		);

		/* Register it. */
		charitable()->donation_fields()->register_field( $field );
	}
);

function allow_custom_donations(){
	//_campaign_allow_custom_donations

	$all_campaigns = new WP_Query(array(
		'post_type' => 'campaign',
		'showposts' => -1,
	));
	if($all_campaigns->have_posts()): while($all_campaigns->have_posts()): $all_campaigns->the_post();
		update_post_meta( get_the_ID(), '_campaign_allow_custom_donations', 1 );
	endwhile; endif; wp_reset_postdata();
}


function get_exired_posts_to_delete()
{
    /**
     * If you need posts that expired more than a week ago, we would need to
     * get the unix time stamp of the day a week ago. You can adjust the relative 
     * date and time formats as needed. 
     * @see http://php.net/manual/en/function.strtotime.php
     * @see http://php.net/manual/en/datetime.formats.php
     */
    // As example, we need to get posts that has expired more than 18mo ago
    $past = strtotime( "- 18 months" );

    // Set our query arguments
    $args = [
        'fields'         => 'ids', // Only get post ID's to improve performance
        'post_type'      => 'dd-registry',
        'post_status'    => 'expired',
        'posts_per_page' => -1,
        'meta_query'     => [
            [
                'key'     => '_expiration_date',
                'value'   => $past,
                'compare' => '<='
            ]
        ]
    ];
    $q = get_posts( $args );

    // Check if we have posts to delete, if not, return false
    if ( !$q )
        return false;

    // OK, we have posts to delete, lets delete them
    foreach ( $q as $id )
        wp_trash_post( $id );
}
// expired_post_delete hook fires when the Cron is executed
add_action( 'expired_post_delete', 'get_exired_posts_to_delete' );

// Add function to register event to wp
add_action( 'wp', 'register_daily_post_delete_event');
function register_daily_post_delete_event() {
    // Make sure this event hasn't been scheduled
    if( !wp_next_scheduled( 'expired_post_delete' ) ) {
        // Schedule the event
        wp_schedule_event( time(), 'daily', 'expired_post_delete' );
    }
}

add_shortcode('user-stripe-connetction', 'ggb_user_stripe_connetction');

function ggb_user_stripe_connetction(){
	if ( is_user_logged_in() ) {
		$stripe_user = new Charitable_Stripe_Connect_User( get_current_user_id() );
		if ( ! $stripe_user->is_user_connected() ) :
			$html = '<div class="stripe-connection-block">';
			$html .= '<p>You haven\'t connected your Stripe account yet.<br/>';
			$html .= '<a href="'.$stripe_user->get_redirect_url().'">Connect to Stripe</a>';
			$html .= '</p></div>';
		endif; 
		return $html;
	}
}

function wpse28782_remove_menu_items() {
    // if( !current_user_can( 'administrator' ) ):
        remove_menu_page( 'edit.php?post_type=dd-donation' );
    //endif;
}
add_action( 'admin_menu', 'wpse28782_remove_menu_items' );