<?php
/*
Template Name: Profile Template
*/
get_header(); ?>
<?php  charitable_stripe()->enqueue_scripts();?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<?php $event = new WP_Query(array(
	'post_type' => 'dd-registry',
	'showposts' => 1,
	'author' => get_current_user_id(),
));
if($event->have_posts()): while($event->have_posts()): $event->the_post();
?>
<div id="registry-id" style="display: none"><?php  echo get_the_ID()?></div>
<?php 
if(get_field('charities') || get_field('charity_1') || get_field('charity_2') || get_field('charity_3')):
if($old_charities = get_field('charities')){
	$charities = $old_charities;
}
global $charity_1;
global $charity_2;
global $charity_3;			
if($charity_1 = get_field('charity_1')){
	$charities[] = get_field('charity_1');
}
if($charity_2 = get_field('charity_2')){
	$charities[] = get_field('charity_2');
}
if($charity_3 = get_field('charity_3')){
	$charities[] = get_field('charity_3');
}
endif?>
<div class="cta-box bgaccent">
	<div class="container flex-wrap">
		<span class="heading">Welcome, <?php the_title();?>!</span>
		<ul class="cta-links">
			<li><a href="#DeleteRegistry" class="btn btn-big btn-del" rel="modal:open">Delete Registry</a></li>
			<li><a href="#EditRegistry" class="btn btn-big btn-edit" rel="modal:open">Edit your info</a></li>	
		</ul>
	</div>
</div>

<div id="DeleteRegistry" class="modal noclose">
	<div class="heading">
		<p class="hcups">REGISTRY FOR <?php the_title();?></p>
	</div>
	<div class="content">
		<p class="hmedium">Do you want to delete your registry?</p>
	</div>
	<div class="buttons toright">
		<?php remove_filter('the_content', 'wpautop');?>
		<?php echo do_shortcode( '[plugin_delete_me /]' ); ?>
		<?php add_filter('the_content', 'wpautop');?>
		<a href="#" class="btn noicon" rel="modal:close">no</a>
	</div>
</div>

<div id="EditRegistry" class="modal">
	<div class="heading">
		<p class="hcups">REGISTRY FOR <?php the_title();?></p>
	</div>
	<div class="content">
		<?php echo do_shortcode('[gravityform id="7" title="false" description="false" update="'.get_the_ID().'"]');?>
	</div>
</div>
<div id="EditCharity" class="modal">
	<div class="heading">
		<p class="hcups">REGISTRY FOR <?php the_title();?></p>
	</div>
	<div class="content">
		<?php echo do_shortcode('[gravityform id="8" title="false" description="false" update="'.get_the_ID().'"]');?>
	</div>
</div>

<div class="container1010 flex-to-right">
	<div class="hero container-small">
		<?php if(has_post_thumbnail()): ?>
		<div class="image">
			<?php the_post_thumbnail('thumb_535x675_true') ; ?>
		</div>
		<?php else: ?>
		<div class="image">
			<img src="<?php site_url();?>/wp-content/uploads/2020/04/TGB_Contact-535x675.jpg" alt="">
		</div>
		<?php endif;?>
		<div class="content">
				<h1 class="hcups">REGISTRY FOR <?php the_title();?></h1>
				<div class="event-data">
					<p class="hmedium">Date: <?php the_field('wedding_date')?></p>
					<p class="hmedium">Location: <?php the_field('wedding_location')?></p>
				</div>
			<a href="#main-content" class="btn">learn more</a>
		</div>
	</div>	
</div>

<div id="main-content" class="type-content bgaccent note">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">A Note From the couple</h2>
				<h3 class="hbig"><?php the_title();?></h3>
			</hgroup>
		</div>
		<div class="content">
			<?php global $current_registry_id ;
			$current_registry_id = get_the_ID();
			echo  apply_filters('the_content', get_field('note'));
			
			$charities_posts = new WP_Query(array(
				'post_type'   => 'dd-charity',
				'post__in' => $charities,
				'orderby' => 'post__in'
			));?>
			<?php if($charities_posts->have_posts()):?>
			<h4 class="hmedium">Our Charities</h4>
				
			<ul class="a-listing">
				<?php while( $charities_posts->have_posts()):  $charities_posts->the_post(); ?>
				<li><a href="<?php the_permalink();?>" target="_blank"><?php the_title();?></a></li>
				<?php endwhile;?>
			</ul>
			<?php endif;wp_reset_postdata();?>
			<a href="#EditCharity" class="btn btn-big btn-fixed btn-edit" rel="modal:open">Edit charity</a>
		</div>	
	</div>
</div>



<div class="type-content">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">The Couple's Favorite Charities</h2>
				<h3 class="hbig">Make a donation</h3>
			</hgroup>
		</div>
		<div class="content">			
			<?php get_template_part('parts/registries-donation-form')?>
		</div>	
	</div>
</div>
<?php $old_donations = get_field('donations', $current_registry_id);?>
<?php $donation_posts = new WP_Query(array(
	'post_type'   => 'donation',
	'showposts' => -1,
	'post_status' => array('charitable-completed','charitable-pending'),
	'meta_key' => 'registry_post_id',
	'meta_value' => $current_registry_id,
	'meta_compare' => 'LIKE',
	'orderby' => 'date',
	'order' => 'ASC'
));
if($donation_posts->have_posts() || $old_donations):
?>
<div class="type-content bgaccent">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">Hurrah!</h2>
				<h3 class="hbig">Your Donors</h3>
			</hgroup>
		</div>
		<div class="content">
			<ul class="a-listing donation-list">
				<li>
					<div class="donor"><strong>Donor</strong></div>
					<div class="charity"><strong>Charity</strong></div>
					<div class="amount"><strong>Amount</strong></div>
				</li>
				<?php if($old_donations):?>
				<?php foreach ( $old_donations as $old_donation ) : ?>
				<li>
					<div class="donor"><?php echo $old_donation['donator']; ?></div>
					<div class="charity"><?php echo $old_donation['charity']; ?></div>
					<div class="amount"><?php echo charitable_format_money( $old_donation['amount']); ?></div>
				</li>
				<?php endforeach;?>
				<?php endif;?>
				<?php while( $donation_posts->have_posts() ): $donation_posts->the_post();?>
					<?php 
					$donation = charitable_get_donation( get_the_ID() );
					$donor       = $donation->get_donor();
					$amount      = $donation->get_total_donation_amount();
					$campaign_donations = $donation->get_campaign_donations();
					?>
				<li>
					<div class="donor"><?php echo $donor->get_name(); ?></div>
					<div class="charity">
						<?php foreach ( $campaign_donations as $campaign_donation ) : ?>
						<?php echo apply_filters( 'charitable_donation_details_table_campaign_donation_campaign', $campaign_donation->campaign_name, $campaign_donation, $donation );?>
						<?php endforeach ?>
					</div>
					<div class="amount"><?php echo charitable_format_money( $amount ); ?></div>
				</li>
				<?php endwhile; ?>
			</ul>
			
		</div>	
	</div>
</div>
<?php endif; wp_reset_postdata()?>
<?php endwhile;?> 
<?php else:?>
<div id="main-content" class="type-content ">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">Not Found</h2>
				<h3 class="hbig">Your Registy not found</h3>
			</hgroup>
		</div>
		<div class="content">
			<p>There is no information about your registry on our site.</p>
		</div>	
	</div>
</div>
<?php endif; wp_reset_postdata();?>
<?php endwhile; endif;?>
<?php get_footer(); ?>