<?php
/*
Template Name: Blog Template
*/
get_header(); ?>


<?php $latest_post = new WP_Query(array(
	'post_type' => 'post',
	'showposts' => 1,
));
if($latest_post->have_posts()): while($latest_post->have_posts()): $latest_post->the_post();?>
<div  class="container1010 flex-to-center" data-aos="fade-up">
	<div class="hero hero-v2 container-small">
		<?php if (has_post_thumbnail()):?>
		<div class="image" data-aos="fade-left">
			<?php the_post_thumbnail('thumb_780x525_true');?>
		</div>
		<?php endif;?>
		<div class="content" data-aos="fade-up" data-aos-duration="500">
			<hgroup>
				<h2 class="hcups">The latest</h2>
				<h1 class="hbig"><a href="<?php the_permalink();?>"><?php the_title()?></a></h1>
			</hgroup>	
			<a href="<?php the_permalink()?>" class="btn">Read more</a>
		</div>
	</div>	
</div>
<?php endwhile; endif; wp_reset_postdata();?>
<?php get_template_part('parts/blog-search-form')?>
<div class="type2" data-aos="fade-up">
	<div class="container">
		<h2 class="hbig">The Archive</h2>	
			<?php 
			 	echo do_shortcode('[ajax_posts]'); 
			?>
	</div>
</div>
<div class="type4" data-aos="fade-up">
	<div class="container">
		<?php if(get_field('fb_image')): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image'), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading')):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading');?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading')):?>
				<h2 class="hbig"><?php the_field('fb_heading');?></h2>
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url') && get_field('fb_cta_button_text')):?>	
			<a href="<?php the_field('fb_cta_button_url');?>" class="btn"><?php the_field('fb_cta_button_text');?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php get_footer(); ?>
