<?php define('WP_USE_THEMES', false);
	require_once('../../../../wp-load.php');

	$charity_id =  $_POST['charity_id'];
	
    $cur_user_id = get_current_user_id();

    $cur_user_registry = new WP_Query(array(
		'post_type'   => 'dd-registry',
		'author'     => $cur_user_id,
		'numberposts' => -1
	));
	$charity_new_added = 0;
    $charity_already_added = 0;
    $charity_list_full = 0;
    if(!get_field('charity_1', $cur_user_registry->posts[0]->ID)){
    	if(get_field('charity_3', $cur_user_registry->posts[0]->ID) == $charity_id || get_field('charity_2', $cur_user_registry->posts[0]->ID) == $charity_id){
    		$charity_already_added = 1;
    	} else {
	    	update_field('charity_1', $charity_id, $cur_user_registry->posts[0]->ID);
	    	$charity_new_added = 1;
	    }
    } elseif (!get_field('charity_2', $cur_user_registry->posts[0]->ID)){
    	if( get_field('charity_3', $cur_user_registry->posts[0]->ID) == $charity_id || get_field('charity_1', $cur_user_registry->posts[0]->ID) == $charity_id){
    		$charity_already_added = 1;
    	} else {
    		update_field('charity_2', $charity_id, $cur_user_registry->posts[0]->ID);
    		$charity_new_added = 1;
    	}
    }  elseif (!get_field('charity_3', $cur_user_registry->posts[0]->ID)){
    	if(get_field('charity_2', $cur_user_registry->posts[0]->ID) == $charity_id || get_field('charity_1', $cur_user_registry->posts[0]->ID) == $charity_id){
    		$charity_already_added = 1;
    	} else {
    		update_field('charity_3', $charity_id, $cur_user_registry->posts[0]->ID);
    		$charity_new_added = 1;
    	}
    } else {
    	$charity_list_full = 1;
    }
    wp_reset_postdata();
?>
<?php if($charity_new_added):?>
<div class="charity-add charity-new-added">
	<p><strong>This charity was successfully added to your charity list.</strong></p>
</div>
<?php endif;?>
<?php if($charity_already_added):?>
<div class="charity-add charity-already-added">
	<p><strong>This charity is already added on your charity list.</strong></p>
</div>
<?php endif;?>
<?php if($charity_list_full):?>
<div class="charity-add charity-already-added">
	<p><strong>This charity cannot be added, your charity list is full. Please remove another charity first.</strong></p>
</div>
<?php endif;?>