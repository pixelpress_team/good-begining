<div class="searchbox bgaccent" data-aos="fade-up">
	<div class="container">
		<div class="container-small">
			<form id="search-registry" action="<?php echo site_url()?>/registry/" class="flex-search flex-wrap">
				<label>find a registry</label>
				<input type="text" placeholder="first name*" name="fn" data-validetta="required" <?php if($_GET['fn']){ echo 'value="'.$_GET['fn'].'"';}?>>
				<input type="text" placeholder="last name*" name="ln" data-validetta="required" <?php if($_GET['ln']){ echo 'value="'.$_GET['ln'].'"';}?>>
				<button class="btn">search</button>
			</form>
		</div>		
	</div>
</div>