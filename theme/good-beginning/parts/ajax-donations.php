<?php define('WP_USE_THEMES', false);
	require_once('../../../../wp-load.php');
?>
<div class="content">
	
	<script type='text/javascript' src='<?php echo site_url();?>/wp-content/plugins/charitable/assets/js/charitable.min.js'></script>


<?php 
	if($_POST['campaign_id']){
		$charityId =  $_POST['campaign_id'];

    
		$charities_posts = get_posts(array(
			'post_type'   => 'dd-charity',
			'include'     => $charityId,
			'numberposts' => -1
		));

		foreach( $charities_posts as $post ){ setup_postdata($post);?>
			<!-- <div class="title" style="display: none"><?php //the_title()?></div> -->
			<?php //if(is_array(get_user_meta($post->post_author, 'stripe_connect_details', true))):
			$campaign_id = get_user_meta( $post->post_author, 'campaign_id',true );
			// else:
			// $campaign_id = 16662;
			// endif;
			echo do_shortcode('[charitable_donation_form campaign_id='.$campaign_id.']');
		}
	}
?>
</div>