<div id="search-filters" class="searchbox bgaccent" data-aos="fade-up">
	<div class="container">
			<form action="<?php echo site_url();?>" class="flex-wrap flex-search search-style2" id="search-form">
				<label>Filter by</label>
				<div id="category" class="select">
					<select name="" id="">
						<option value="category">Category</option>
						<?php $terms = get_terms( 'dd-cause', [
							'order' => ASC,
						] );
						$active_term = get_queried_object();?>
						<?php foreach( $terms as $term ):?>
						<option value="<?php echo get_term_link($term, 'dd-cause');?>" <?php if($active_term->term_id == $term->term_id){ echo 'selected';}?>><?php echo $term->name;?></option>
						<?php endforeach;?>
					</select>	
				</div>
				<?php $locations = get_terms_by_level('location');?>
				<?php $selected_locations = selected_locations()?>
				<div id="location-country" class="select">
					<select name="" id="">
						<option value="category">Country</option>						
						<?php foreach( $locations['top_lvl'] as $top_level_term ):?>
						<option value="<?php echo get_term_link($top_level_term, 'location');?>" <?php if(in_array( $top_level_term->term_id, $selected_locations)){ echo 'selected';}?>><?php echo $top_level_term->name;?></option>
						<?php endforeach;?>						
					</select>	
				</div>
				<div id="location-state" class="select">
					<select name="" id="">
						<option value="category">State</option>
						<?php foreach( $locations['second_lvl'] as $second_level_term ):?>
						<option value="<?php echo get_term_link($second_level_term, 'location');?>"  <?php if(in_array( $second_level_term->term_id, $selected_locations)){ echo 'selected';}?>><?php echo $second_level_term->name;?></option>
						<?php endforeach;?>						
					</select>	
				</div>
				<div  id="location-city" class="select">
					<select name="" id="">
						<option value="category">City</option>
						<?php foreach( $locations['third_lvl'] as $third_level_term ):?>
						<option value="<?php echo get_term_link($third_level_term, 'location');?>" <?php if(in_array( $third_level_term->term_id, $selected_locations)){ echo 'selected';}?>><?php echo $third_level_term->name;?></option>
						<?php endforeach;?>		
					</select>	
				</div>
			</form>
	</div>
</div>