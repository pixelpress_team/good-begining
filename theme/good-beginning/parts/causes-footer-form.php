<div class="container1010 flex-wrap" data-aos="fade-up">
	<div class="heading">
		<hgroup>
			<h2 class="hcups">Suggest a Charity</h2>
			<h3 class="hbig">Feel free to add your organization of choice.</h3>
		</hgroup>
	</div>
	<div class="content">
		<p>Complete the form to add the organization, and we will do all the backend paperwork needed to get everything set up to receive gifts.</p>
		<!--<form action=""  class="form-flex">
			<input class="size-onehalf" type="text" placeholder="Your Name">
			<input class="size-onehalf" type="email" placeholder="Email">
			<input class="size-full" type="text" placeholder="Charity Name">
			<input class="size-full" type="text" placeholder="Charity Website">
			<input type="submit" value="+ Submit your suggestion" class="btn btn-big">
		</form>	-->
		<?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]'); ?>	
	</div>	
</div>