<div class="form-flex form-borders" data-aos="fade-up">
	<div class="select">
		<?php global $current_registry_id;?>
		<?php if( empty($current_registry_id)) {$current_registry_id = get_the_ID();}?>
		<?php if($old_charities = get_field('charities', $current_registry_id)){
			$couple_charities = $old_charities;
		}			
		if(get_field('charity_1',$current_registry_id)){
			$couple_charities[] = get_field('charity_1',$current_registry_id);
		}
		if(get_field('charity_2',$current_registry_id)){
			$couple_charities[] = get_field('charity_2',$current_registry_id);
		}
		if(get_field('charity_3',$current_registry_id)){
			$couple_charities[] = get_field('charity_3',$current_registry_id);
		}?>
		<?php $charities_posts = get_posts(array(
				'post_type'   => 'dd-charity',
				'include'     => $couple_charities,
				'numberposts' => -1
			));?>

		<select name="" id="charities">
			<option value="">Select a Charity</option>
			<?php foreach( $charities_posts as $post ){ setup_postdata($post);?>
			<option value="<?php echo get_the_ID()?>"><?php the_title()?></option>
			<?php
			}
			wp_reset_postdata();
			?>
		</select>
	</div>
	<a href="#DonationForm" class="btn btn-big btn-edit" id="donation-popup" rel="modal:open">MAKE A DONATION</a>
</div>
<?php if(!is_single(16684)):?>
<div id="DonationForm" class="modal">
</div>
<?php endif?>