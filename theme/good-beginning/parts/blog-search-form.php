<div id="top" class="searchbox bgaccent blog-search" data-aos="fade-up">
	<div class="container">
		<form action="<?php echo site_url();?>" class="flex-wrap flex-search" id="search-form">
			<label>Search by</label>
			<div id="category" class="select">
				<select >
					<option value="">Category</option>
					<?php $categories = get_categories([
						'order'        => 'ASC',
					]);?>
					<?php foreach( $categories as $cat ):?>
					<option value="<?php echo get_term_link($cat, 'category');?>" <?php if($_GET['category'] == $cat->term_id){ echo 'selected';}?>><?php echo $cat->name;?></option>
					<?php endforeach;?>

				</select>	
			</div>
			<div id="date" class="select">
				<select>
					<option value="">Archive</option>
					<?php wp_get_archives(array('format' => 'option'));?>
				</select>	
			</div>
			<div id="keyword">
				<input type="text" placeholder="keyword" name="s">
				 <input type="hidden" name="post_type" value="post" />
			</div>
			<!-- <button class="btn">search</button> -->
		</form>
	</div>
</div>