<?php get_header(); ?>
<?php  charitable_stripe()->enqueue_scripts();?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div id="registry-id" style="display: none"><?php  echo get_the_ID()?></div>
<div class="container1010 flex-to-right">
	<div class="hero container-small">
		<?php if(has_post_thumbnail()): ?>
		<div class="image">
			<?php the_post_thumbnail('thumb_535x675_true') ; ?>
		</div>
		<?php else: ?>
		<div class="image">
			<img src="<?php site_url();?>/wp-content/uploads/2020/04/TGB_Contact-535x675.jpg" alt="">
		</div>
		<?php endif;?>
		<div class="content">
				<h1 class="hcups">REGISTRY FOR <?php the_title();?></h1>
				<div class="event-data">
					<p class="hmedium">Date: <?php the_field('wedding_date')?></p>
					<p class="hmedium">Location: <?php the_field('wedding_location')?></p>
					<?php if(get_field('wedding_website')):?><p><a href="<?php the_field('wedding_website'); ?>" style="text-decoration:none;" target="_blank" class="hmedium">View Website</a></p><?php endif; ?>
				</div>
			<a href="#main-content" class="btn">learn more</a>
		</div>
	</div>	
</div>
<div id="main-content" class="type-content bgaccent note ">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">A Note From the couple</h2>
				<h3 class="hbig"><?php the_title();?></h3>
			</hgroup>
		</div>
		<div class="content">
			<?php echo  apply_filters('the_content', get_field('note'))?>
			<?php 
			if(get_field('charities') || get_field('charity_1') || get_field('charity_2') || get_field('charity_3')):
			if($old_charities = get_field('charities')){
				$charities = $old_charities;
			}			
			if(get_field('charity_1')){
				$charities[] = get_field('charity_1');
			}
			if(get_field('charity_2')){
				$charities[] = get_field('charity_2');
			}
			if(get_field('charity_3')){
				$charities[] = get_field('charity_3');
			}
			$charities_posts = get_posts(array(
				'post_type'   => 'dd-charity',
				'include' => $charities,
			));?>
			<h4 class="hmedium">Our Charities</h4>

			<ul class="a-listing">
				<?php foreach( $charities_posts as $post ){ setup_postdata($post);?>
				<li><a href="<?php the_permalink();?>" target="_blank"><?php the_title();?></a></li>
				<?php
				}
				wp_reset_postdata();
				?>
			</ul>
			<?php endif;?>
			<a href="#make-donation" class="btn btn-big">Make a donation</a>
		</div>	
	</div>
</div>
<div id="make-donation" class="type-content">
	<div class="container1010 flex-wrap">
		<div class="heading">
			<hgroup>
				<h2 class="hcups">The Couple's Favorite Charities</h2>
				<h3 class="hbig">Make a donation</h3>
			</hgroup>
		</div>
		<div class="content">
			<?php get_template_part('parts/registries-donation-form')?>
		</div>	
	</div>
</div>
<?php endwhile; endif;?>

<?php get_footer(); ?>
