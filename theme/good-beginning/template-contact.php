<?php
/*
Template Name: Contact Template
*/
get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="container" data-aos="fade-up">
	<div class="hero hero-v2 container-small">
		<?php if(get_field('promo_image')): ?>
		<div class="image" data-aos="fade-left">
			<?php echo wp_get_attachment_image( get_field('promo_image'), 'thumb_768x517_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content padding-same">
			<hgroup>
				<?php if(get_field('promo_sub-heading')):?>
				<h2 class="hcups"><?php the_field('promo_sub-heading');?></h2>
				<?php endif;?>
				<?php if(get_field('promo_heading')):?>
				<h1 class="hbig"><?php the_field('promo_heading');?></h1>
				<?php endif;?>
			</hgroup>	
			<div>
				<ul class="a-listing">
					<?php if(get_field('promo_email')):?>
					<li><a href="mailto:<?php the_field('promo_email')?>"><?php the_field('promo_email')?></a></li>
					<?php endif;?>
					<?php if($phone = get_field('promo_phone')):?>
					<li><a href="tel:+<?php echo preg_replace('/[^0-9]/','', $phone);?>"><?php echo $phone;?></a></li>
					<?php endif;?>
					<?php //if(get_field('promo_visit_link')):?>
					<!-- <li><a href="<?php //the_field('promo_visit_link')?>">Visit (By Appointment Only)</a></li> -->
					<?php //endif;?>
					<?php if(have_rows('promo_address')):?>
					<li class="text">
						<?php while(have_rows('promo_address')): the_row()?>
						<span class="box"><?php the_sub_field('line')?></span>
						<?php endwhile;?>
					</li>
					<?php endif;?>
				</ul>
			</div>
		</div>
	</div>	
</div>


<div class="type4" data-aos="fade-up">
	<div class="container">
		<?php if(get_field('fb_image')): ?>
		<div class="image">
			<?php echo wp_get_attachment_image( get_field('fb_image'), 'thumb_1120x565_true' ); ?>
		</div>
		<?php endif;?>
		<div class="content">
			<hgroup>
				<?php if(get_field('fb_sub-heading')):?>
				<h3 class="hcups"><?php the_field('fb_sub-heading');?></h3>
				<?php endif;?>
				<?php if(get_field('fb_heading')):?>
				<h2 class="hbig"><?php the_field('fb_heading');?></h2
				<?php endif;?>
			</hgroup>
			<?php if(get_field('fb_cta_button_url') && get_field('fb_cta_button_text')):?>	
			<a href="<?php the_field('fb_cta_button_url');?>" class="btn"><?php the_field('fb_cta_button_text');?></a>
			<?php endif;?>
		</div>	
	</div>
</div>
<?php endwhile; endif;?>
<?php get_footer(); ?>
